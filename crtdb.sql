## Redaxo Database Dump Version 5
## Prefix rex_
## charset utf-8

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `rex_action`;
CREATE TABLE `rex_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `preview` text,
  `presave` text,
  `postsave` text,
  `previewmode` tinyint(4) DEFAULT NULL,
  `presavemode` tinyint(4) DEFAULT NULL,
  `postsavemode` tinyint(4) DEFAULT NULL,
  `createuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `updatedate` datetime NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_article`;
CREATE TABLE `rex_article` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `catpriority` int(10) unsigned NOT NULL,
  `startarticle` tinyint(1) NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `path` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `clang_id` int(10) unsigned NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  `yrewrite_url` varchar(255) NOT NULL,
  `yrewrite_canonical_url` varchar(255) NOT NULL,
  `yrewrite_priority` varchar(5) NOT NULL,
  `yrewrite_changefreq` varchar(10) NOT NULL,
  `yrewrite_title` varchar(255) NOT NULL,
  `yrewrite_description` text NOT NULL,
  `yrewrite_index` tinyint(1) NOT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `find_articles` (`id`,`clang_id`),
  KEY `id` (`id`),
  KEY `clang_id` (`clang_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_article` WRITE;
/*!40000 ALTER TABLE `rex_article` DISABLE KEYS */;
INSERT INTO `rex_article` VALUES 
  (1,1,0,'Inicio','',0,0,1,'|',1,'2018-07-21 10:20:54','2018-07-27 17:12:41',1,1,'admin','admin',0,'','','','','','',0),
  (2,2,0,'Nosotros','',0,0,2,'|',1,'2018-07-21 11:46:10','2018-08-07 12:26:55',4,1,'admin','admin-cromotecnica',0,'','','','','','',0),
  (3,3,0,'Contactos','',0,0,3,'|',1,'2018-07-21 11:46:27','2018-07-21 12:29:46',4,1,'admin','admin',0,'','','','','','',0),
  (4,4,0,'Slider','',0,0,4,'|',1,'2018-07-21 12:17:38','2018-08-27 17:08:51',5,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (5,5,0,'Muebles para el hogar','Muebles para el hogar',1,1,1,'|',1,'2018-07-23 09:53:17','2018-08-08 17:50:58',6,1,'admin','admin',0,'','','','','','',0),
  (6,6,0,'Muebles de oficina','Muebles de oficina',2,1,1,'|',1,'2018-07-23 09:53:50','2018-07-28 09:25:11',6,1,'admin','admin',0,'','','','','','',0),
  (7,7,0,'Muebles hospitalarios','Muebles hospitalarios',3,1,1,'|',1,'2018-07-23 11:21:39','2018-08-08 12:51:54',6,1,'admin','admin',0,'','','','','','',0),
  (8,8,0,'Muebles educativos','Muebles educativos',4,1,1,'|',1,'2018-07-23 11:21:45','2018-08-08 16:21:41',6,1,'admin','admin',0,'','','','','','',0),
  (9,9,0,'Cromados para vehículos','Cromados para vehículos',6,1,1,'|',1,'2018-07-23 11:21:51','2018-08-21 17:28:50',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (10,10,0,'Parachoques y Parrillas','Parachoques y Parrillas',7,1,1,'|',1,'2018-07-23 09:55:30','2018-08-21 18:13:18',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (11,11,0,'Servicios','Servicios',8,1,1,'|',1,'2018-07-23 10:32:15','2018-08-07 17:46:25',6,1,'admin','admin-cromotecnica',0,'','','','','','',0),
  (12,12,5,'Juegos de comedor','Juegos de comedor',1,1,2,'|5|',1,'2018-07-23 16:09:31','2018-08-21 17:43:33',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (13,13,5,'Juegos de living','Juegos de living',2,1,3,'|5|',1,'2018-07-23 16:09:47','2018-07-28 12:30:14',6,1,'admin','admin',0,'','','','','','',0),
  (14,14,5,'Mesas centrales de living','Mesas centrales de living',3,1,4,'|5|',1,'2018-07-23 16:10:01','2018-08-07 16:46:15',6,1,'admin','admin-cromotecnica',0,'','','','','','',0),
  (15,15,5,'Mesas esquineras de living','Mesas esquineras de living',4,1,5,'|5|',1,'2018-07-23 16:12:07','2018-08-07 16:45:46',6,1,'admin','admin-cromotecnica',0,'','','','','','',0),
  (16,16,38,'Sillas','Sillas',1,1,6,'|38|',1,'2018-07-23 16:12:53','2018-08-21 18:09:53',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (17,17,38,'Taburetes','Taburetes',3,1,7,'|38|',1,'2018-07-23 16:13:10','2018-08-21 17:45:02',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (19,18,6,'Escritorios','Escritorios',1,1,1,'|6|',1,'2018-07-24 09:16:49','2018-07-24 09:16:33',6,1,'admin','admin',0,'','','','','','',0),
  (20,19,6,'Mesas de reuniones','Mesas de reuniones',2,1,1,'|6|',1,'2018-07-24 09:23:20','2018-07-24 09:16:45',6,1,'admin','admin',0,'','','','','','',0),
  (21,20,6,'Sillas ejecutivas','Sillas ejecutivas',3,1,1,'|6|',1,'2018-07-24 17:33:13','2018-07-24 09:24:41',6,1,'admin','admin',0,'','','','','','',0),
  (22,21,6,'Sillones','Sillones',4,1,1,'|6|',1,'2018-07-24 17:33:16','2018-07-24 09:24:51',6,1,'admin','admin',0,'','','','','','',0),
  (23,22,7,'Sillas de espera','Sillas de espera',1,1,1,'|7|',1,'2018-07-24 17:33:52','2018-07-24 17:33:50',6,1,'admin','admin',0,'','','','','','',0),
  (24,23,38,'Butacas(Tandems)','Butacas(Tandems)',2,1,1,'|38|',1,'2018-07-24 17:34:03','2018-08-21 18:08:34',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (25,24,7,'Camillas','Camillas',2,1,1,'|7|',1,'2018-07-24 17:34:12','2018-07-24 17:34:10',6,1,'admin','admin',0,'','','','','','',0),
  (26,25,7,'Camas','Camas',3,1,1,'|7|',1,'2018-07-24 17:34:20','2018-07-24 17:34:18',6,1,'admin','admin',0,'','','','','','',0),
  (27,26,7,'Biombos(Separadores)','Biombos(Separadores)',4,1,1,'|7|',1,'2018-07-24 17:34:29','2018-07-24 17:34:28',6,1,'admin','admin',0,'','','','','','',0),
  (28,27,8,'Pupitres','Pupitres',1,1,1,'|8|',1,'2018-07-24 17:34:44','2018-08-08 16:25:17',6,1,'admin','admin',0,'','','','','','',0),
  (29,28,8,'Mesas de estudio','Mesas de estudio',2,1,1,'|8|',1,'2018-07-24 17:34:53','2018-08-08 17:58:01',6,1,'admin','admin',0,'','','','','','',0),
  (31,30,8,'Mesas de trabajo','Mesas de trabajo',4,1,1,'|8|',1,'2018-07-24 17:35:23','2018-08-08 17:58:25',6,1,'admin','admin',0,'','','','','','',0),
  (32,31,9,'Barras anti-vuelcos','Barras anti-vuelcos',1,1,1,'|9|',1,'2018-07-24 17:35:59','2018-08-21 17:22:59',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (33,32,9,'Barras laterales','Barras laterales',2,1,1,'|9|',1,'2018-07-24 17:36:11','2018-08-21 17:49:10',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (34,33,9,'Pisaderas','Pisaderas',3,1,1,'|9|',1,'2018-07-24 17:36:26','2018-08-21 17:27:11',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (36,34,10,'Parachoques','Parachoques',1,1,1,'|10|',1,'2018-07-24 17:37:24','2018-08-13 18:17:21',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (37,35,10,'Parrillas','Parrillas',2,1,1,'|10|',1,'2018-07-24 17:37:24','2018-08-21 18:06:55',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (38,36,11,'Cromados','Cromados',1,1,1,'|11|',1,'2018-07-24 17:37:47','2018-08-07 18:02:50',6,1,'admin','admin-cromotecnica',0,'','','','','','',0),
  (39,37,11,'Pintura electrostática','Pintura electrostática',2,1,1,'|11|',1,'2018-07-24 18:02:09','2018-08-07 17:50:32',6,1,'admin','admin-cromotecnica',0,'','','','','','',0),
  (40,38,0,'Sillas, Tandems y Taburetes','Sillas, Tandems y Taburetes',5,1,1,'|',1,'2018-08-08 11:44:07','2018-08-21 17:47:28',6,1,'admin','soporte-cromotecnica',0,'','','','','','',0),
  (43,39,8,'Sillas escolares','Sillas escolares',3,1,1,'|8|',1,'2018-08-08 17:56:29','2018-08-08 17:57:20',6,1,'admin','admin',0,'','','','','','',0);
/*!40000 ALTER TABLE `rex_article` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_article_slice`;
CREATE TABLE `rex_article_slice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clang_id` int(10) unsigned NOT NULL,
  `ctype_id` int(10) unsigned NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `value1` text,
  `value2` text,
  `value3` text,
  `value4` text,
  `value5` text,
  `value6` text,
  `value7` text,
  `value8` text,
  `value9` text,
  `value10` text,
  `value11` text,
  `value12` text,
  `value13` text,
  `value14` text,
  `value15` text,
  `value16` text,
  `value17` text,
  `value18` text,
  `value19` text,
  `value20` text,
  `media1` varchar(255) DEFAULT NULL,
  `media2` varchar(255) DEFAULT NULL,
  `media3` varchar(255) DEFAULT NULL,
  `media4` varchar(255) DEFAULT NULL,
  `media5` varchar(255) DEFAULT NULL,
  `media6` varchar(255) DEFAULT NULL,
  `media7` varchar(255) DEFAULT NULL,
  `media8` varchar(255) DEFAULT NULL,
  `media9` varchar(255) DEFAULT NULL,
  `media10` varchar(255) DEFAULT NULL,
  `medialist1` text,
  `medialist2` text,
  `medialist3` text,
  `medialist4` text,
  `medialist5` text,
  `medialist6` text,
  `medialist7` text,
  `medialist8` text,
  `medialist9` text,
  `medialist10` text,
  `link1` varchar(10) DEFAULT NULL,
  `link2` varchar(10) DEFAULT NULL,
  `link3` varchar(10) DEFAULT NULL,
  `link4` varchar(10) DEFAULT NULL,
  `link5` varchar(10) DEFAULT NULL,
  `link6` varchar(10) DEFAULT NULL,
  `link7` varchar(10) DEFAULT NULL,
  `link8` varchar(10) DEFAULT NULL,
  `link9` varchar(10) DEFAULT NULL,
  `link10` varchar(10) DEFAULT NULL,
  `linklist1` text,
  `linklist2` text,
  `linklist3` text,
  `linklist4` text,
  `linklist5` text,
  `linklist6` text,
  `linklist7` text,
  `linklist8` text,
  `linklist9` text,
  `linklist10` text,
  `article_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `revision` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slice_priority` (`article_id`,`priority`,`module_id`),
  KEY `clang_id` (`clang_id`),
  KEY `article_id` (`article_id`),
  KEY `find_slices` (`clang_id`,`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_article_slice` WRITE;
/*!40000 ALTER TABLE `rex_article_slice` DISABLE KEYS */;
INSERT INTO `rex_article_slice` VALUES 
  (1,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',2,3,'2018-07-21 12:16:47','2018-08-07 12:26:55','admin','admin-cromotecnica',0),
  (3,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',3,2,'2018-07-21 12:29:11','2018-07-21 12:29:11','admin','admin',0),
  (13,1,1,2,'','','','','','','','','','','','','','','','','','','','','slide03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,6,'2018-07-26 17:52:39','2018-08-08 17:14:57','admin','admin',0),
  (14,1,1,3,'','','','','','','','','','','','','','','','','','','','','slide06.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,6,'2018-07-26 17:52:50','2018-08-20 16:43:40','admin','admin',0),
  (15,1,1,4,'','','','','','','','','','','','','','','','','','','','','slide01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,6,'2018-07-26 17:53:04','2018-07-26 17:53:04','admin','admin',0),
  (16,1,1,6,'','','','','','','','','','','','','','','','','','','','','slide04.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,6,'2018-07-26 17:53:31','2018-07-26 17:53:31','admin','admin',0),
  (17,1,1,5,'','','','','','','','','','','','','','','','','','','','','slide02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,6,'2018-07-26 17:53:51','2018-08-27 17:08:51','admin','soporte-cromotecnica',0),
  (18,1,1,7,'','','','','','','','','','','','','','','','','','','','','slide05.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,6,'2018-07-26 17:54:10','2018-08-20 16:52:31','admin','admin',0),
  (22,1,1,1,'','','','','','','','','','','','','','','','','','','','','','slide06.jpg','living.jpg','mesas_centrales_de_living.jpg','mesas_esquineras_de_living.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',5,4,'2018-07-28 00:11:08','2018-08-08 17:50:58','admin','admin',0),
  (24,1,1,1,'','','','','','','','','','','','','','','','','','','','','','silla-de-espera.jpg','camillas.jpg','camas.jpg','biombos_separadores.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',7,4,'2018-07-28 00:45:01','2018-08-08 12:51:54','admin','admin',0),
  (25,1,1,1,'','','','','','','','','','','','','','','','','','','','','','slide01.jpg','','slide01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',8,4,'2018-07-28 00:45:43','2018-08-08 16:21:40','admin','admin',0),
  (26,1,1,1,'','','','','','','','','','','','','','','','','','','','','','barra_antivuelco_-_1.jpg','','pisaderas_-_3.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',9,4,'2018-07-28 00:45:55','2018-08-21 17:28:50','admin','soporte-cromotecnica',0),
  (27,1,1,1,'','','','','','','','','','','','','','','','','','','','','','dsc_0227.jpg','parrilla_-_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',10,4,'2018-07-28 00:46:17','2018-08-21 18:13:18','admin','soporte-cromotecnica',0),
  (28,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',6,4,'2018-07-28 09:25:11','2018-07-28 09:25:11','admin','admin',0),
  (33,1,1,1,'<p>Los juegos de comedor son más que sólo muebles, son testigo de innumerables recuerdos y momentos de risa. Para servir una mesa elegante a tus amigos o para vivir la rutina del día a día con la familia, contar con un juego de comedor es fundamental para decorar y revivir el espíritu del hogar.</p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',12,8,'2018-07-28 11:46:33','2018-07-28 11:46:33','admin','admin',0),
  (34,1,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',13,7,'2018-07-28 12:29:55','2018-07-28 12:29:55','admin','admin',0),
  (35,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',13,7,'2018-07-28 12:30:01','2018-07-28 12:30:01','admin','admin',0),
  (36,1,1,3,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',13,7,'2018-07-28 12:30:10','2018-07-28 12:30:10','admin','admin',0),
  (37,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',13,7,'2018-07-28 12:30:14','2018-07-28 12:30:14','admin','admin',0),
  (43,1,1,1,'','','','','','','','','','','','','','','','','','','','','lateral_3.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',4,6,'2018-08-07 16:27:51','2018-08-07 16:27:51','admin-cromotecnica','admin-cromotecnica',0),
  (51,1,1,1,'<p>La pintura electrostática en polvo, es la forma moderna de recubrir con alta calidad y bajo costo. &nbsp;Tiene ventajas contra la pintura tradicional, al no utilizar solventes, tener tiempos de proceso más bajos, poder producir capas gruesas en una sola aplicación sin posibilidad de escurrimientos, y reducir al mínimo el desecho de pintura. Adicionalmente su pueden lograr acabados resistentes al rayado, a la corrosión, a la interperie, la abrasión, el impacto y los químicos.</p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',37,8,'2018-08-07 17:50:32','2018-08-07 17:50:32','admin-cromotecnica','admin-cromotecnica',0),
  (52,1,1,1,'<p class=\"text-center\">En&nbsp;<strong>Cromotécnica Boliviana.</strong>&nbsp;ofrecemos el&nbsp;<strong>servicio de cromado de metales</strong>, hasta con cuatro capas de cobre, níquel y cromo, lo que garantiza un alto micraje y años de duración.<br></p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',36,8,'2018-08-07 17:54:38','2018-08-07 17:56:40','admin-cromotecnica','admin-cromotecnica',0),
  (53,1,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',36,7,'2018-08-07 17:54:47','2018-08-07 17:54:47','admin-cromotecnica','admin-cromotecnica',0),
  (54,1,1,3,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',36,7,'2018-08-07 17:54:53','2018-08-07 17:56:15','admin-cromotecnica','admin-cromotecnica',0),
  (55,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',36,7,'2018-08-07 17:54:56','2018-08-07 17:54:56','admin-cromotecnica','admin-cromotecnica',0),
  (56,1,1,6,'<p class=\"text-center\"><em>Para presupuestos también&nbsp;puede enviar fotos&nbsp;con las medidas y peso aproximado a nuestro</em> <strong><a href=\"https://api.whatsapp.com/send?phone=59170717027\" target=\"_blank\">Whatsapp&nbsp;70 71 70 27</a></strong></p>','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',36,8,'2018-08-07 17:55:52','2018-08-07 18:02:50','admin-cromotecnica','admin-cromotecnica',0),
  (57,1,1,5,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',36,7,'2018-08-07 18:01:27','2018-08-07 18:01:27','admin-cromotecnica','admin-cromotecnica',0),
  (58,1,1,1,'','','','','','','','','','','','','','','','','','','','','','slide03.jpg','butaca02_1.jpg','slide02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',38,4,'2018-08-08 11:55:07','2018-08-21 17:47:28','admin','soporte-cromotecnica',0),
  (59,1,1,1,'','','','','','','','','','','','','','','','','','','','','butaca01_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',23,7,'2018-08-08 12:56:49','2018-08-08 12:56:49','admin','admin',0),
  (60,1,1,2,'','','','','','','','','','','','','','','','','','','','','butaca02_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',23,7,'2018-08-08 12:56:58','2018-08-08 12:57:22','admin','admin',0),
  (61,1,1,1,'','','','','','','','','','','','','','','','','','','','','escolar01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',27,7,'2018-08-08 16:25:00','2018-08-08 16:25:00','admin','admin',0),
  (62,1,1,2,'','','','','','','','','','','','','','','','','','','','','escolar02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',27,7,'2018-08-08 16:25:09','2018-08-08 16:25:09','admin','admin',0),
  (63,1,1,3,'','','','','','','','','','','','','','','','','','','','','escolar03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',27,7,'2018-08-08 16:25:17','2018-08-08 16:25:17','admin','admin',0),
  (64,1,1,2,'','','','','','','','','','','','','','','','','','','','','comedor01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',12,7,'2018-08-08 17:51:20','2018-08-08 17:53:34','admin','admin',0),
  (65,1,1,3,'','','','','','','','','','','','','','','','','','','','','comedor03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',12,7,'2018-08-08 17:51:24','2018-08-08 17:53:43','admin','admin',0),
  (66,1,1,4,'','','','','','','','','','','','','','','','','','','','','comedor02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',12,7,'2018-08-08 17:51:28','2018-08-08 17:53:51','admin','admin',0),
  (67,1,1,5,'','','','','','','','','','','','','','','','','','','','','silla_11.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',12,7,'2018-08-08 17:51:32','2018-08-21 17:43:33','admin','soporte-cromotecnica',0),
  (68,1,1,1,'','','','','','','','','','','','','','','','','','','','','escolar01.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',39,7,'2018-08-08 17:56:29','2018-08-08 17:56:29','admin','admin',0),
  (69,1,1,2,'','','','','','','','','','','','','','','','','','','','','escolar02.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',39,7,'2018-08-08 17:56:29','2018-08-08 17:56:29','admin','admin',0),
  (70,1,1,3,'','','','','','','','','','','','','','','','','','','','','escolar03.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',39,7,'2018-08-08 17:56:29','2018-08-08 17:56:29','admin','admin',0),
  (71,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',28,7,'2018-08-08 17:57:46','2018-08-08 17:57:46','admin','admin',0),
  (72,1,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',28,7,'2018-08-08 17:57:53','2018-08-08 17:57:53','admin','admin',0),
  (73,1,1,3,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',28,7,'2018-08-08 17:57:57','2018-08-08 17:57:57','admin','admin',0),
  (74,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',28,7,'2018-08-08 17:58:01','2018-08-08 17:58:01','admin','admin',0),
  (75,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',30,7,'2018-08-08 17:58:11','2018-08-08 17:58:11','admin','admin',0),
  (76,1,1,3,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',30,7,'2018-08-08 17:58:16','2018-08-08 17:58:16','admin','admin',0),
  (77,1,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',30,7,'2018-08-08 17:58:21','2018-08-08 17:58:21','admin','admin',0),
  (78,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',30,7,'2018-08-08 17:58:25','2018-08-08 17:58:25','admin','admin',0),
  (79,1,1,1,'','','','','','','','','','','','','','','','','','','','','dsc_0227.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',34,7,'2018-08-13 17:47:08','2018-08-13 17:47:08','soporte-cromotecnica','soporte-cromotecnica',0),
  (80,1,1,2,'','','','','','','','','','','','','','','','','','','','','dsc_0230.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',34,7,'2018-08-13 17:47:19','2018-08-13 17:47:19','soporte-cromotecnica','soporte-cromotecnica',0),
  (81,1,1,3,'','','','','','','','','','','','','','','','','','','','','dsc_0178_5.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',34,7,'2018-08-13 18:17:21','2018-08-13 18:17:21','soporte-cromotecnica','soporte-cromotecnica',0),
  (82,1,1,1,'','','','','','','','','','','','','','','','','','','','','barra_antivuelco_-_3.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',31,7,'2018-08-21 17:19:25','2018-08-21 17:22:08','soporte-cromotecnica','soporte-cromotecnica',0),
  (83,1,1,2,'','','','','','','','','','','','','','','','','','','','','barra_antivuelco_-_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',31,7,'2018-08-21 17:19:34','2018-08-21 17:22:49','soporte-cromotecnica','soporte-cromotecnica',0),
  (84,1,1,3,'','','','','','','','','','','','','','','','','','','','','barra_antivuelco_-_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',31,7,'2018-08-21 17:19:38','2018-08-21 17:22:59','soporte-cromotecnica','soporte-cromotecnica',0),
  (85,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',31,7,'2018-08-21 17:19:42','2018-08-21 17:19:42','soporte-cromotecnica','soporte-cromotecnica',0),
  (86,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',33,7,'2018-08-21 17:26:28','2018-08-21 17:26:28','soporte-cromotecnica','soporte-cromotecnica',0),
  (87,1,1,3,'','','','','','','','','','','','','','','','','','','','','pisaderas_-_2.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',33,7,'2018-08-21 17:26:32','2018-08-21 17:27:11','soporte-cromotecnica','soporte-cromotecnica',0),
  (88,1,1,1,'','','','','','','','','','','','','','','','','','','','','pisaderas_-_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',33,7,'2018-08-21 17:26:37','2018-08-21 17:26:53','soporte-cromotecnica','soporte-cromotecnica',0),
  (89,1,1,2,'','','','','','','','','','','','','','','','','','','','','pisaderas_-_3.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',33,7,'2018-08-21 17:26:41','2018-08-21 17:27:03','soporte-cromotecnica','soporte-cromotecnica',0),
  (90,1,1,2,'','','','','','','','','','','','','','','','','','','','','silla_12.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',16,7,'2018-08-21 17:35:09','2018-08-21 17:46:58','soporte-cromotecnica','soporte-cromotecnica',0),
  (91,1,1,1,'','','','','','','','','','','','','','','','','','','','','silla_brazo_redondo.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',16,7,'2018-08-21 17:35:13','2018-08-21 17:46:49','soporte-cromotecnica','soporte-cromotecnica',0),
  (92,1,1,3,'','','','','','','','','','','','','','','','','','','','','silla_platino_-_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',16,7,'2018-08-21 17:35:17','2018-08-21 18:09:28','soporte-cromotecnica','soporte-cromotecnica',0),
  (93,1,1,4,'','','','','','','','','','','','','','','','','','','','','silla_ovalada_agarrador_-_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',16,7,'2018-08-21 17:35:24','2018-08-21 18:09:53','soporte-cromotecnica','soporte-cromotecnica',0),
  (94,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',17,7,'2018-08-21 17:43:56','2018-08-21 17:43:56','soporte-cromotecnica','soporte-cromotecnica',0),
  (95,1,1,1,'','','','','','','','','','','','','','','','','','','','','taburete_cajero_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',17,7,'2018-08-21 17:44:00','2018-08-21 17:44:34','soporte-cromotecnica','soporte-cromotecnica',0),
  (96,1,1,2,'','','','','','','','','','','','','','','','','','','','','taburete_mod_nuevo_chico_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',17,7,'2018-08-21 17:44:04','2018-08-21 17:45:02','soporte-cromotecnica','soporte-cromotecnica',0),
  (97,1,1,3,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',17,7,'2018-08-21 17:44:07','2018-08-21 17:44:07','soporte-cromotecnica','soporte-cromotecnica',0),
  (98,1,1,3,'','','','','','','','','','','','','','','','','','','','','sillas_tandems_-1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',23,7,'2018-08-21 17:47:53','2018-08-21 18:08:34','soporte-cromotecnica','soporte-cromotecnica',0),
  (99,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',23,7,'2018-08-21 17:47:57','2018-08-21 17:47:57','soporte-cromotecnica','soporte-cromotecnica',0),
  (100,1,1,3,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',32,7,'2018-08-21 17:48:57','2018-08-21 17:48:57','soporte-cromotecnica','soporte-cromotecnica',0),
  (101,1,1,1,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',32,7,'2018-08-21 17:49:01','2018-08-21 17:49:01','soporte-cromotecnica','soporte-cromotecnica',0),
  (102,1,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',32,7,'2018-08-21 17:49:05','2018-08-21 17:49:05','soporte-cromotecnica','soporte-cromotecnica',0),
  (103,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',32,7,'2018-08-21 17:49:10','2018-08-21 17:49:10','soporte-cromotecnica','soporte-cromotecnica',0),
  (104,1,1,4,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',35,7,'2018-08-21 18:05:25','2018-08-21 18:05:25','soporte-cromotecnica','soporte-cromotecnica',0),
  (105,1,1,1,'','','','','','','','','','','','','','','','','','','','','parrilla_-_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',35,7,'2018-08-21 18:05:29','2018-08-21 18:06:46','soporte-cromotecnica','soporte-cromotecnica',0),
  (106,1,1,2,'','','','','','','','','','','','','','','','','','','','','parrilla_escalera_-_1.jpg','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',35,7,'2018-08-21 18:05:33','2018-08-21 18:06:55','soporte-cromotecnica','soporte-cromotecnica',0),
  (107,1,1,3,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',35,7,'2018-08-21 18:05:37','2018-08-21 18:05:37','soporte-cromotecnica','soporte-cromotecnica',0);
/*!40000 ALTER TABLE `rex_article_slice` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_clang`;
CREATE TABLE `rex_clang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_clang` WRITE;
/*!40000 ALTER TABLE `rex_clang` DISABLE KEYS */;
INSERT INTO `rex_clang` VALUES 
  (1,'es','español',1,1,0);
/*!40000 ALTER TABLE `rex_clang` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_config`;
CREATE TABLE `rex_config` (
  `namespace` varchar(75) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`namespace`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rex_config` WRITE;
/*!40000 ALTER TABLE `rex_config` DISABLE KEYS */;
INSERT INTO `rex_config` VALUES 
  ('be_style/customizer','codemirror','1'),
  ('be_style/customizer','codemirror-langs','0'),
  ('be_style/customizer','codemirror-tools','0'),
  ('be_style/customizer','codemirror_theme','\"eclipse\"'),
  ('be_style/customizer','labelcolor','\"#43a047\"'),
  ('be_style/customizer','showlink','1'),
  ('core','package-config','{\"backup\":{\"install\":true,\"status\":true},\"be_style\":{\"install\":true,\"status\":true,\"plugins\":{\"customizer\":{\"install\":true,\"status\":true},\"redaxo\":{\"install\":true,\"status\":true}}},\"cronjob\":{\"install\":false,\"status\":false,\"plugins\":{\"article_status\":{\"install\":false,\"status\":false},\"optimize_tables\":{\"install\":false,\"status\":false}}},\"focuspoint\":{\"install\":false,\"status\":false},\"install\":{\"install\":true,\"status\":true},\"media_manager\":{\"install\":true,\"status\":true},\"mediapool\":{\"install\":true,\"status\":true},\"metainfo\":{\"install\":true,\"status\":true},\"phpmailer\":{\"install\":true,\"status\":true},\"project\":{\"install\":true,\"status\":true},\"quick_navigation\":{\"install\":true,\"status\":true},\"structure\":{\"install\":true,\"status\":true,\"plugins\":{\"content\":{\"install\":true,\"status\":true},\"history\":{\"install\":false,\"status\":false},\"version\":{\"install\":false,\"status\":false}}},\"uploader\":{\"install\":true,\"status\":true},\"users\":{\"install\":true,\"status\":true},\"yform\":{\"install\":true,\"status\":true,\"plugins\":{\"docs\":{\"install\":true,\"status\":true},\"email\":{\"install\":true,\"status\":true},\"manager\":{\"install\":true,\"status\":true},\"tools\":{\"install\":false,\"status\":false}}},\"yrewrite\":{\"install\":true,\"status\":true}}'),
  ('core','package-order','[\"be_style\",\"be_style\\/customizer\",\"be_style\\/redaxo\",\"users\",\"backup\",\"install\",\"media_manager\",\"mediapool\",\"phpmailer\",\"structure\",\"uploader\",\"metainfo\",\"quick_navigation\",\"structure\\/content\",\"yform\",\"yform\\/docs\",\"yform\\/email\",\"yform\\/manager\",\"yrewrite\",\"project\"]'),
  ('core','version','\"5.6.2\"'),
  ('media_manager','interlace','[\"jpg\"]'),
  ('media_manager','jpg_quality','85'),
  ('media_manager','png_compression','5'),
  ('media_manager','webp_quality','85'),
  ('phpmailer','bcc','\"\"'),
  ('phpmailer','charset','\"utf-8\"'),
  ('phpmailer','confirmto','\"\"'),
  ('phpmailer','encoding','\"8bit\"'),
  ('phpmailer','from','\"\"'),
  ('phpmailer','fromname','\"Mailer\"'),
  ('phpmailer','host','\"localhost\"'),
  ('phpmailer','log','0'),
  ('phpmailer','mailer','\"mail\"'),
  ('phpmailer','password','\"\"'),
  ('phpmailer','port','25'),
  ('phpmailer','priority','0'),
  ('phpmailer','smtpauth','false'),
  ('phpmailer','smtpsecure','\"\"'),
  ('phpmailer','smtp_debug','\"0\"'),
  ('phpmailer','test_address','\"\"'),
  ('phpmailer','username','\"\"'),
  ('phpmailer','wordwrap','120'),
  ('quick_navigation','quicknavi_favs1','[]'),
  ('structure','notfound_article_id','1'),
  ('structure','start_article_id','1'),
  ('structure/content','default_template_id','1'),
  ('uploader','image-max-filesize','30'),
  ('uploader','image-max-height','4000'),
  ('uploader','image-max-width','4000'),
  ('uploader','image-resize-checked','true');
/*!40000 ALTER TABLE `rex_config` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_media`;
CREATE TABLE `rex_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `attributes` text,
  `filetype` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `originalname` varchar(255) DEFAULT NULL,
  `filesize` varchar(255) DEFAULT NULL,
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_media` WRITE;
/*!40000 ALTER TABLE `rex_media` DISABLE KEYS */;
INSERT INTO `rex_media` VALUES 
  (7,1,'','image/jpeg','slide01.jpg','slide01.jpg','59502',1400,460,'MUEBLES EDUCATIVOS','2018-07-26 17:49:11','2018-07-26 17:51:50','admin','admin',0),
  (8,1,'','image/jpeg','slide02.jpg','slide02.jpg','60546',1400,460,'TABURETES','2018-07-26 17:49:11','2018-08-27 17:08:46','admin','soporte-cromotecnica',0),
  (9,1,'','image/jpeg','slide03.jpg','slide03.jpg','47924',1400,460,'SILLAS','2018-07-26 17:49:11','2018-08-08 17:14:54','admin','admin',0),
  (10,1,'','image/jpeg','slide04.jpg','slide04.jpg','72347',1400,460,'PARACHOQUES DELANTEROS','2018-07-26 17:49:11','2018-07-27 16:59:10','admin','admin',0),
  (11,1,'','image/jpeg','slide05.jpg','slide05.jpg','61087',1400,460,'PARACHOQUES TRASEROS','2018-07-26 17:49:11','2018-07-27 16:58:34','admin','admin',0),
  (12,1,'','image/jpeg','slide06.jpg','slide06.jpg','78516',1400,460,'JUEGOS DE COMEDOR','2018-07-26 17:49:11','2018-08-20 16:43:35','admin','admin',0),
  (14,3,'','image/jpeg','living.jpg','living.jpg','46575',708,405,'','2018-07-28 10:06:43','2018-07-28 10:06:43','admin','admin',0),
  (15,4,'','image/jpeg','mesas_centrales_de_living.jpg','mesas_centrales_de_living.jpg','41501',738,495,'Mesas centrales de living','2018-07-28 10:11:45','2018-07-28 10:11:45','admin','admin',0),
  (17,5,'','image/jpeg','mesas_esquineras_de_living.jpg','mesas_esquineras_de_living.jpg','26574',695,394,'Mesas esquineras de living','2018-07-28 10:19:56','2018-07-28 10:19:56','admin','admin',0),
  (30,7,'','image/jpeg','dsc03380.jpg','dsc03380.jpg','85899',768,576,'Silla de espera','2018-07-30 16:30:17','2018-07-30 16:30:17','admin','admin',0),
  (31,7,'','image/jpeg','biombos_separadores.jpg','biombos_separadores.jpg','48256',750,552,'Muebles Hospitalarios','2018-07-30 16:48:09','2018-07-30 16:48:09','admin','admin',0),
  (32,7,'','image/png','butacas_tandems.png','butacas_tandems.png','203036',800,403,'Muebles Hospitalarios','2018-07-30 16:48:09','2018-07-30 16:48:09','admin','admin',0),
  (33,7,'','image/jpeg','camillas.jpg','camillas.jpg','242870',1969,1476,'Muebles Hospitalarios','2018-07-30 16:48:09','2018-07-30 16:48:09','admin','admin',0),
  (34,7,'','image/jpeg','silla-de-espera.jpg','silla-de-espera.jpg','33327',566,268,'Muebles Hospitalarios','2018-07-30 16:48:09','2018-07-30 16:48:09','admin','admin',0),
  (35,7,'','image/jpeg','camas.jpg','camas.jpg','115596',1200,674,'Muebles Hospitalarios','2018-07-30 16:48:09','2018-07-30 16:48:09','admin','admin',0),
  (43,1,'','image/jpeg','lateral_3.jpg','lateral_3.jpg','3727638',3840,2160,'CROMOTÉCNICA BOLIVIANA','2018-08-07 16:27:46','2018-08-07 16:27:46','admin-cromotecnica','admin-cromotecnica',0),
  (48,9,'','image/jpeg','taburete_alto_pintado.jpg','taburete_alto_pintado.jpg','943838',2160,3272,'Taburete para el hogar','2018-08-07 17:11:00','2018-08-07 17:34:40','admin-cromotecnica','admin-cromotecnica',0),
  (49,9,'','image/jpeg','taburete_cajero.jpg','taburete_cajero.jpg','3096738',2448,3264,'Taburete para el hogar','2018-08-07 17:11:00','2018-08-07 17:11:00','admin-cromotecnica','admin-cromotecnica',0),
  (50,9,'','image/jpeg','taburete_fijo.jpg','taburete_fijo.jpg','943838',2160,3272,'Taburete para el hogar','2018-08-07 17:11:00','2018-08-07 17:34:56','admin-cromotecnica','admin-cromotecnica',0),
  (51,9,'','image/jpeg','taburete_mod_nuevo_chico.jpg','taburete_mod_nuevo_chico.jpg','3057005',3264,2448,'Taburete para el hogar','2018-08-07 17:11:01','2018-08-07 17:11:01','admin-cromotecnica','admin-cromotecnica',0),
  (54,10,'','image/jpeg','butaca01_1.jpg','butaca01_1.jpg','30321',830,590,'Butacas (Tandems)','2018-08-08 12:56:44','2018-08-08 12:56:44','admin','admin',0),
  (55,10,'','image/jpeg','butaca02_1.jpg','butaca02_1.jpg','28450',830,590,'Butacas (Tandems)','2018-08-08 12:56:45','2018-08-08 12:56:45','admin','admin',0),
  (58,11,'','image/jpeg','escolar01.jpg','escolar01.jpg','22639',830,590,'Pupitre','2018-08-08 16:22:51','2018-08-08 16:22:51','admin','admin',0),
  (61,11,'','image/jpeg','escolar02.jpg','escolar02.jpg','27598',830,590,'Pupitre','2018-08-08 16:24:51','2018-08-08 16:24:51','admin','admin',0),
  (62,11,'','image/jpeg','escolar03.jpg','escolar03.jpg','26972',830,590,'Pupitre','2018-08-08 16:24:51','2018-08-08 16:24:51','admin','admin',0),
  (63,2,'','image/jpeg','comedor01.jpg','comedor01.jpg','41606',830,590,'Juego de comedor','2018-08-08 17:53:30','2018-08-08 17:53:30','admin','admin',0),
  (64,2,'','image/jpeg','comedor03.jpg','comedor03.jpg','38813',830,590,'Juego de comedor','2018-08-08 17:53:30','2018-08-08 17:53:30','admin','admin',0),
  (65,2,'','image/jpeg','comedor02.jpg','comedor02.jpg','49932',830,590,'Juego de comedor','2018-08-08 17:53:30','2018-08-08 17:53:30','admin','admin',0),
  (66,8,'','image/jpeg','dsc_0230.jpg','dsc_0230.jpg','266942',4000,956,'Parachoque delantero Chevrolet','2018-08-13 17:47:04','2018-08-21 17:18:06','soporte-cromotecnica','soporte-cromotecnica',0),
  (67,8,'','image/jpeg','dsc_0227.jpg','dsc_0227.jpg','472176',4000,1661,'Parachoque delantero Chevrolet','2018-08-13 17:47:05','2018-08-21 17:17:39','soporte-cromotecnica','soporte-cromotecnica',0),
  (68,8,'','image/jpeg','dsc_0178_5.jpg','dsc_0178_5.jpg','233706',2848,1152,'Parachoque delantero Volvo','2018-08-13 18:17:12','2018-08-21 17:17:55','soporte-cromotecnica','soporte-cromotecnica',0),
  (69,12,'','image/jpeg','barra_antivuelco_-_2.jpg','barra_antivuelco_-_2.jpg','1209437',3280,2120,'Barra antivuelco','2018-08-21 17:21:47','2018-08-21 17:21:47','soporte-cromotecnica','soporte-cromotecnica',0),
  (70,12,'','image/jpeg','barra_antivuelco_-_1.jpg','barra_antivuelco_-_1.jpg','1785905',3636,2376,'Barra antivuelco','2018-08-21 17:21:48','2018-08-21 17:21:48','soporte-cromotecnica','soporte-cromotecnica',0),
  (71,12,'','image/jpeg','barra_antivuelco_-_3.jpg','barra_antivuelco_-_3.jpg','799444',3264,1488,'Barra antivuelco','2018-08-21 17:21:49','2018-08-21 17:21:49','soporte-cromotecnica','soporte-cromotecnica',0),
  (72,12,'','image/jpeg','pisaderas_-_2.jpg','pisaderas_-_2.jpg','43379',664,304,'Pisadera','2018-08-21 17:25:56','2018-08-21 17:25:56','soporte-cromotecnica','soporte-cromotecnica',0),
  (73,12,'','image/jpeg','pisaderas_-_3.jpg','pisaderas_-_3.jpg','1114442',3136,2376,'Pisadera','2018-08-21 17:26:02','2018-08-21 17:26:02','soporte-cromotecnica','soporte-cromotecnica',0),
  (74,12,'','image/jpeg','pisaderas_-_1.jpg','pisaderas_-_1.jpg','929385',3336,2000,'Pisadera','2018-08-21 17:26:02','2018-08-21 17:26:02','soporte-cromotecnica','soporte-cromotecnica',0),
  (75,2,'','image/jpeg','silla_14.jpg','silla_14.jpg','63188',900,506,'','2018-08-21 17:37:29','2018-08-21 17:39:38','soporte-cromotecnica','soporte-cromotecnica',0),
  (76,2,'','image/jpeg','silla_11.jpg','silla_11.jpg','380436',3000,2387,'Juego de comedor','2018-08-21 17:37:33','2018-08-21 17:41:35','soporte-cromotecnica','soporte-cromotecnica',0),
  (77,10,'','image/jpeg','silla_12.jpg','silla_12.jpg','219744',720,446,'4 Sillas de colores','2018-08-21 17:37:34','2018-08-21 17:42:08','soporte-cromotecnica','soporte-cromotecnica',0),
  (78,10,'','image/jpeg','silla_brazo_redondo.jpg','silla_brazo_redondo.jpg','425848',3000,4000,'Silla con brazo redondo','2018-08-21 17:37:40','2018-08-21 17:42:32','soporte-cromotecnica','soporte-cromotecnica',0),
  (79,10,'','image/jpeg','taburete_mod_nuevo_chico_1.jpg','taburete_mod_nuevo_chico_1.jpg','249335',1547,2154,'Taburete pequeño','2018-08-21 17:43:23','2018-08-21 17:44:58','soporte-cromotecnica','soporte-cromotecnica',0),
  (80,10,'','image/jpeg','taburete_cajero_1.jpg','taburete_cajero_1.jpg','429841',1968,3484,'Taburete cajero','2018-08-21 17:43:24','2018-08-21 17:44:30','soporte-cromotecnica','soporte-cromotecnica',0),
  (81,8,'','image/jpeg','parrilla_-_1.jpg','parrilla_-_1.jpg','654352',3264,1144,'Parrilla','2018-08-21 18:06:27','2018-08-21 18:06:27','soporte-cromotecnica','soporte-cromotecnica',0),
  (82,8,'','image/jpeg','parrilla_escalera_-_1.jpg','parrilla_escalera_-_1.jpg','763112',3264,2200,'Parrilla','2018-08-21 18:06:28','2018-08-21 18:06:28','soporte-cromotecnica','soporte-cromotecnica',0),
  (83,10,'','image/jpeg','sillas_tandems_-1.jpg','sillas_tandems_-1.jpg','258479',1307,654,'Tandem','2018-08-21 18:08:02','2018-08-21 18:08:30','soporte-cromotecnica','soporte-cromotecnica',0),
  (84,10,'','image/jpeg','silla_platino_-_1.jpg','silla_platino_-_1.jpg','693490',3000,4000,'Silla platino','2018-08-21 18:09:23','2018-08-21 18:09:23','soporte-cromotecnica','soporte-cromotecnica',0),
  (85,10,'','image/jpeg','silla_ovalada_agarrador_-_1.jpg','silla_ovalada_agarrador_-_1.jpg','1452197',2160,3840,'Silla ovalada','2018-08-21 18:09:50','2018-08-21 18:09:50','soporte-cromotecnica','soporte-cromotecnica',0);
/*!40000 ALTER TABLE `rex_media` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_media_category`;
CREATE TABLE `rex_media_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `path` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `attributes` text,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_media_category` WRITE;
/*!40000 ALTER TABLE `rex_media_category` DISABLE KEYS */;
INSERT INTO `rex_media_category` VALUES 
  (1,'Banners para Slider',0,'|','2018-07-26 11:11:16','2018-07-26 11:11:16','admin','admin','',0),
  (2,'Juegos de comedor',0,'|','2018-07-28 09:28:21','2018-07-28 09:28:21','admin','admin','',0),
  (3,'Juegos de living',0,'|','2018-07-28 09:43:33','2018-07-28 09:43:33','admin','admin','',0),
  (4,'Mesas centrales de living',0,'|','2018-07-28 10:11:27','2018-07-28 10:11:27','admin','admin','',0),
  (5,'Mesas esquineras de living',0,'|','2018-07-28 10:18:33','2018-07-28 10:18:33','admin','admin','',0),
  (7,'Muebles hospitalarios',0,'|','2018-07-30 16:28:47','2018-07-30 16:28:47','admin','admin','',0),
  (8,'Parachoques y parrillas',0,'|','2018-07-30 16:58:35','2018-07-30 16:58:35','admin','admin','',0),
  (9,'Taburetes',0,'|','2018-08-07 17:01:17','2018-08-07 17:01:17','admin-cromotecnica','admin-cromotecnica','',0),
  (10,'Sillas, Tandems y Taburetes',0,'|','2018-08-08 12:55:21','2018-08-08 12:55:21','admin','admin','',0),
  (11,'Muebles educativos',0,'|','2018-08-08 16:22:23','2018-08-08 16:22:23','admin','admin','',0),
  (12,'Cromados para vehículos',0,'|','2018-08-21 17:20:32','2018-08-21 17:20:32','soporte-cromotecnica','soporte-cromotecnica','',0);
/*!40000 ALTER TABLE `rex_media_category` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_media_manager_type`;
CREATE TABLE `rex_media_manager_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_media_manager_type` WRITE;
/*!40000 ALTER TABLE `rex_media_manager_type` DISABLE KEYS */;
INSERT INTO `rex_media_manager_type` VALUES 
  (1,1,'rex_mediapool_detail','Zur Darstellung von Bildern in der Detailansicht im Medienpool'),
  (2,1,'rex_mediapool_maximized','Zur Darstellung von Bildern im Medienpool wenn maximiert'),
  (3,1,'rex_mediapool_preview','Zur Darstellung der Vorschaubilder im Medienpool'),
  (4,1,'rex_mediabutton_preview','Zur Darstellung der Vorschaubilder in REX_MEDIA_BUTTON[]s'),
  (5,1,'rex_medialistbutton_preview','Zur Darstellung der Vorschaubilder in REX_MEDIALIST_BUTTON[]s'),
  (6,0,'slide1420w',''),
  (7,0,'slide960w',''),
  (8,0,'slide768w',''),
  (9,0,'banner300h',''),
  (10,0,'thumb-210',''),
  (11,0,'thumb-850','');
/*!40000 ALTER TABLE `rex_media_manager_type` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_media_manager_type_effect`;
CREATE TABLE `rex_media_manager_type_effect` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL,
  `effect` varchar(255) NOT NULL,
  `parameters` text NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `updatedate` datetime NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `createuser` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_media_manager_type_effect` WRITE;
/*!40000 ALTER TABLE `rex_media_manager_type_effect` DISABLE KEYS */;
INSERT INTO `rex_media_manager_type_effect` VALUES 
  (1,1,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"200\",\"rex_effect_resize_height\":\"200\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (2,2,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"600\",\"rex_effect_resize_height\":\"600\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (3,3,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"80\",\"rex_effect_resize_height\":\"80\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (4,4,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"246\",\"rex_effect_resize_height\":\"246\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (5,5,'resize','{\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_amount\":\"80\",\"rex_effect_filter_blur_radius\":\"8\",\"rex_effect_filter_blur_threshold\":\"3\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"246\",\"rex_effect_resize_height\":\"246\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"}}',1,'0000-00-00 00:00:00','','0000-00-00 00:00:00',''),
  (6,6,'resize','{\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"\",\"rex_effect_resize_height\":\"480\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"}}',1,'2018-07-26 11:03:27','admin','2018-07-26 11:03:27','admin'),
  (7,7,'resize','{\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"\",\"rex_effect_resize_height\":\"440\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"}}',1,'2018-07-26 11:04:02','admin','2018-07-26 11:04:02','admin'),
  (8,8,'resize','{\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"\",\"rex_effect_resize_height\":\"340\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"}}',1,'2018-07-26 11:04:33','admin','2018-07-26 11:04:33','admin'),
  (9,9,'resize','{\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"\",\"rex_effect_resize_height\":\"400\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"}}',1,'2018-07-28 09:37:21','admin','2018-07-28 09:11:28','admin'),
  (10,10,'resize','{\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"300\",\"rex_effect_resize_height\":\"\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"}}',1,'2018-08-07 17:27:35','admin-cromotecnica','2018-07-30 10:12:29','admin'),
  (11,11,'resize','{\"rex_effect_convert2img\":{\"rex_effect_convert2img_convert_to\":\"jpg\",\"rex_effect_convert2img_density\":\"150\"},\"rex_effect_header\":{\"rex_effect_header_download\":\"open_media\",\"rex_effect_header_cache\":\"no_cache\"},\"rex_effect_filter_brightness\":{\"rex_effect_filter_brightness_brightness\":\"\"},\"rex_effect_filter_contrast\":{\"rex_effect_filter_contrast_contrast\":\"\"},\"rex_effect_flip\":{\"rex_effect_flip_flip\":\"X\"},\"rex_effect_crop\":{\"rex_effect_crop_width\":\"\",\"rex_effect_crop_height\":\"\",\"rex_effect_crop_offset_width\":\"\",\"rex_effect_crop_offset_height\":\"\",\"rex_effect_crop_hpos\":\"center\",\"rex_effect_crop_vpos\":\"middle\"},\"rex_effect_filter_sharpen\":{\"rex_effect_filter_sharpen_amount\":\"80\",\"rex_effect_filter_sharpen_radius\":\"0.5\",\"rex_effect_filter_sharpen_threshold\":\"3\"},\"rex_effect_mirror\":{\"rex_effect_mirror_height\":\"\",\"rex_effect_mirror_set_transparent\":\"colored\",\"rex_effect_mirror_bg_r\":\"\",\"rex_effect_mirror_bg_g\":\"\",\"rex_effect_mirror_bg_b\":\"\"},\"rex_effect_filter_colorize\":{\"rex_effect_filter_colorize_filter_r\":\"\",\"rex_effect_filter_colorize_filter_g\":\"\",\"rex_effect_filter_colorize_filter_b\":\"\"},\"rex_effect_image_properties\":{\"rex_effect_image_properties_jpg_quality\":\"\",\"rex_effect_image_properties_png_compression\":\"\",\"rex_effect_image_properties_webp_quality\":\"\",\"rex_effect_image_properties_interlace\":null},\"rex_effect_filter_blur\":{\"rex_effect_filter_blur_repeats\":\"10\",\"rex_effect_filter_blur_type\":\"gaussian\",\"rex_effect_filter_blur_smoothit\":\"\"},\"rex_effect_resize\":{\"rex_effect_resize_width\":\"\",\"rex_effect_resize_height\":\"850\",\"rex_effect_resize_style\":\"maximum\",\"rex_effect_resize_allow_enlarge\":\"not_enlarge\"},\"rex_effect_workspace\":{\"rex_effect_workspace_width\":\"\",\"rex_effect_workspace_height\":\"\",\"rex_effect_workspace_hpos\":\"left\",\"rex_effect_workspace_vpos\":\"top\",\"rex_effect_workspace_set_transparent\":\"colored\",\"rex_effect_workspace_bg_r\":\"\",\"rex_effect_workspace_bg_g\":\"\",\"rex_effect_workspace_bg_b\":\"\"},\"rex_effect_rounded_corners\":{\"rex_effect_rounded_corners_topleft\":\"\",\"rex_effect_rounded_corners_topright\":\"\",\"rex_effect_rounded_corners_bottomleft\":\"\",\"rex_effect_rounded_corners_bottomright\":\"\"},\"rex_effect_insert_image\":{\"rex_effect_insert_image_brandimage\":\"\",\"rex_effect_insert_image_hpos\":\"left\",\"rex_effect_insert_image_vpos\":\"top\",\"rex_effect_insert_image_padding_x\":\"-10\",\"rex_effect_insert_image_padding_y\":\"-10\"},\"rex_effect_rotate\":{\"rex_effect_rotate_rotate\":\"0\"},\"rex_effect_mediapath\":{\"rex_effect_mediapath_mediapath\":\"\"}}',1,'2018-07-30 10:13:06','admin','2018-07-30 10:13:06','admin');
/*!40000 ALTER TABLE `rex_media_manager_type_effect` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_metainfo_field`;
CREATE TABLE `rex_metainfo_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `priority` int(10) unsigned NOT NULL,
  `attributes` text NOT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `default` varchar(255) NOT NULL,
  `params` text,
  `validate` text,
  `callback` text,
  `restrictions` text,
  `createuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `updatedate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_metainfo_type`;
CREATE TABLE `rex_metainfo_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `dbtype` varchar(255) NOT NULL,
  `dblength` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_metainfo_type` WRITE;
/*!40000 ALTER TABLE `rex_metainfo_type` DISABLE KEYS */;
INSERT INTO `rex_metainfo_type` VALUES 
  (1,'text','text',0),
  (2,'textarea','text',0),
  (3,'select','varchar',255),
  (4,'radio','varchar',255),
  (5,'checkbox','varchar',255),
  (6,'REX_MEDIA_WIDGET','varchar',255),
  (7,'REX_MEDIALIST_WIDGET','text',0),
  (8,'REX_LINK_WIDGET','varchar',255),
  (9,'REX_LINKLIST_WIDGET','text',0),
  (10,'date','text',0),
  (11,'datetime','text',0),
  (12,'legend','text',0),
  (13,'time','text',0);
/*!40000 ALTER TABLE `rex_metainfo_type` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_module`;
CREATE TABLE `rex_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `output` mediumtext NOT NULL,
  `input` mediumtext NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `attributes` text,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_module` WRITE;
/*!40000 ALTER TABLE `rex_module` DISABLE KEYS */;
INSERT INTO `rex_module` VALUES 
  (2,'Contactos','<div class=\"crt-container\">\n            <div class=\"crt-item\">\n              <h2 class=\"main-main__title center\">Contáctenos</h2>\n            </div>\n            <div class=\"s-30 crt-item l-block\">\n              <form class=\"crt-container form\" v-on:submit.prevent=\"submitForm\">\n                <div class=\"crt-item form__item\">\n                  <label class=\"form__label\" for=\"\">Nombre completo: </label>\n                  <input class=\"form__input\" v-model=\"vue.nombre\" type=\"text\" required=\"\">\n                </div>\n                <div class=\"crt-item form__item\">\n                  <label class=\"form__label\" for=\"\">Teléfono: </label>\n                  <input class=\"form__input\" v-model=\"vue.telefono\" type=\"number\">\n                </div>\n                <div class=\"crt-item form__item\">\n                  <label class=\"form__label\" for=\"\">Celular: </label>\n                  <input class=\"form__input\" v-model=\"vue.movil\" type=\"number\" required=\"\">\n                </div>\n                <div class=\"crt-item form__item\">\n                  <label class=\"form__label\" for=\"\">Dirección: </label>\n                  <input class=\"form__input\" v-model=\"vue.direccion\" type=\"text\" required=\"\">\n                </div>\n                <div class=\"crt-item form__item\">\n                  <label class=\"form__label\" for=\"\">Ciudad: </label>\n                  <input class=\"form__input\" v-model=\"vue.ciudad\" type=\"text\" required=\"\">\n                </div>\n                <div class=\"crt-item form__item\">\n                  <label class=\"form__label\" for=\"\">Email: </label>\n                  <input class=\"form__input\" v-model=\"vue.email\" type=\"email\" required=\"\">\n                </div>\n                <div class=\"crt-item form__item\">\n                  <label class=\"form__label\" for=\"\">Mensaje: </label>\n                  <textarea class=\"form__input\" v-model=\"vue.mensaje\" name=\"\" cols=\"30\" rows=\"10\" required=\"\"></textarea>\n                </div>\n                <div class=\"crt-item form__item center\">\n                  <button class=\"button--\" type=\"submit\">Enviar</button>\n                  <button class=\"button--\" v-on:click=\"clearForm()\">Limpiar</button>\n                </div>\n                <div class=\"statusMessage\" v-if=\"formSubmitted\">\n                  <h4 v-text=\"vue.envio\"></h4>\n                </div>\n              </form>\n            </div>\n            <div class=\"s-70 crt-item l-block\">\n              <h3 class=\"main-main__subtitle center\">Ubíquenos</h3><iframe src=\"https://www.google.com/maps/d/embed?mid=1F-66rtK-O5rfIMNJqaCz_bDtNCU\" width=\"100%\" height=\"480\"></iframe>\n            </div>\n          </div>','SALIDA DE FORMULARIO DE CONTACTOS Y MAPAS','admin','admin','2018-07-21 12:09:58','2018-07-21 12:11:58','',0),
  (3,'Nosotros','<div class=\"crt-container\">\r\n  <div class=\"crt-item\">\r\n    <h2 class=\"main-main__title center\">Nosotros</h2>\r\n  </div>\r\n  <div class=\"crt-item l-45\">\r\n    <h3 class=\"main-main__subtitle center\">Misión</h3>\r\n    <p>Fabricar y Diseñar muebles de alta calidad acorde a las exigencias y tendencias de clientes y mercado, garantizado calidad en nuestros productos y satisfacción a nuestros clientes.</p>\r\n    <h3 class=\"main-main__subtitle center\">Visión</h3>\r\n    <p>Posicionar en el mercado Boliviano muebles con calidad y precios competitivos con un fuerte posicionamiento y satisfacción de nuestros clientes.</p>\r\n  </div>\r\n  <div class=\"crt-item l-55\">\r\n    <iframe width=\"100%\" height=\"315\" src=\"https://www.youtube.com/embed/NEzJSU3BKXA?rel=0\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>\r\n  </div>\r\n</div>','<div class=\"crt-container\">\r\n  <div class=\"crt-item\">\r\n    <h2 class=\"main-main__title center\">Nosotros</h2>\r\n  </div>\r\n  <div class=\"crt-item l-45\">\r\n    <h3 class=\"main-main__subtitle center\">Misión</h3>\r\n    <p>Fabricar y Diseñar muebles de alta calidad acorde a las exigencias y tendencias de clientes y mercado, garantizado calidad en nuestros productos y satisfacción a nuestros clientes.</p>\r\n    <h3 class=\"main-main__subtitle center\">Visión</h3>\r\n    <p>Posicionar en el mercado Boliviano muebles con calidad y precios competitivos con un fuerte posicionamiento y satisfacción de nuestros clientes.</p>\r\n  </div>\r\n  <div class=\"crt-item l-55\">\r\n    <iframe width=\"100%\" height=\"315\" src=\"https://www.youtube.com/embed/NEzJSU3BKXA?rel=0\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>\r\n  </div>\r\n</div>','admin','admin','2018-07-21 12:10:08','2018-07-25 18:03:04','',0),
  (4,'Animated Banner','<?php       \r\n  $path = explode(\"|\",$this->getValue(\"path\").$this->getValue(\"article_id\").\"|\");\r\n  $path1 = ((!empty($path[1])) ? $path[1] : \'\');\r\n  $path2 = ((!empty($path[2])) ? $path[2] : \'\');\r\n  $banner_main = \'\';\r\n  $md=1;\r\n  $mediatype= \'banner300h\';\r\n  $banner = [\r\n    REX_MEDIA[id=1],\r\n    REX_MEDIA[id=2],\r\n    REX_MEDIA[id=3],\r\n    REX_MEDIA[id=4],\r\n    REX_MEDIA[id=5],\r\n    REX_MEDIA[id=6],\r\n    REX_MEDIA[id=7],\r\n    REX_MEDIA[id=8],\r\n    REX_MEDIA[id=9]\r\n  ];\r\n  // START 2nd level categories\r\n  foreach (rex_category::getCurrent()->getChildren() as $lev2):\r\n    $category_is_visible = true;\r\n    // only visible if online_from and online_to is ok\r\n    if ($lev2->getValue(\'art_online_from\') != \'\' && $lev2->getValue(\'art_online_from\') > time()) { $category_is_visible = false; }\r\n    if ($lev2->getValue(\'art_online_to\') != \'\' && $lev2->getValue(\'art_online_to\') < time()) { $category_is_visible = false; }\r\n    if ($lev2->isOnline(true) && $category_is_visible == true) {\r\n      if (\'REX_MEDIA[\' . $md . \']\' != \'\'){\r\n        $banner300 = \'index.php?rex_media_type=\' . $mediatype . \'&rex_media_file=\' . $banner[$md] . \'\';\r\n      }else{\r\n        $banner300 = \'https://images.pexels.com/photos/245208/pexels-photo-245208.jpeg?cs=srgb&amp;dl=architecture-cabinets-carpet-245208.jpg&amp;fm=jpg\';\r\n      }\r\n      $banner_main .= \'\r\n      <a class=\"banner__item\" href=\"#\'.$lev2->getId().\'\" style=\"display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;height: 300px;background-repeat: no-repeat;    background-size: cover; background-position: center; background-image: url(\'. $banner300 .\');\">\r\n        <h2 class=\"banner__title\">\'.htmlspecialchars($lev2->getValue(\'name\')).\'</h2></a>\';\r\n      $md=$md+1;\r\n    }\r\n  endforeach;\r\n  $banner_main .= \'\';\r\n  echo \'\r\n    <div class=\"crt-item banner l-block\">\r\n      \'.$banner_main.\'\r\n      \'.rex::getProperty(\'lang_switch\').\'                        \r\n    </div>\r\n    \';\r\n?> ','<?php       \r\n  $path = explode(\"|\",$this->getValue(\"path\").$this->getValue(\"article_id\").\"|\");\r\n  $path1 = ((!empty($path[1])) ? $path[1] : \'\');\r\n  $path2 = ((!empty($path[2])) ? $path[2] : \'\');\r\n  $banner_main = \'\';\r\n  $md=1;\r\n  $banner = [\r\n    REX_MEDIA[id=1 widget=1],\r\n    REX_MEDIA[id=2 widget=1],\r\n    REX_MEDIA[id=3 widget=1],\r\n    REX_MEDIA[id=4 widget=1],\r\n    REX_MEDIA[id=5 widget=1],\r\n    REX_MEDIA[id=6 widget=1],\r\n    REX_MEDIA[id=7 widget=1],\r\n    REX_MEDIA[id=8 widget=1],\r\n    REX_MEDIA[id=9 widget=1]\r\n  ];\r\n  // START 2nd level categories\r\n  foreach (rex_category::getCurrent()->getChildren() as $lev2):                      \r\n    $category_is_visible = true;\r\n    // only visible if online_from and online_to is ok\r\n    if ($lev2->getValue(\'art_online_from\') != \'\' && $lev2->getValue(\'art_online_from\') > time()) { $category_is_visible = false; }\r\n    if ($lev2->getValue(\'art_online_to\') != \'\' && $lev2->getValue(\'art_online_to\') < time()) { $category_is_visible = false; }\r\n    if ($lev2->isOnline(true) && $category_is_visible == true) {\r\n      $banner_main .= \'\r\n      <a class=\"banner__item\" href=\"#\'.$lev2->getId().\'\" style=\"background-image: url(https://images.pexels.com/photos/245208/pexels-photo-245208.jpeg?cs=srgb&amp;dl=architecture-cabinets-carpet-245208.jpg&amp;fm=jpg);\">\r\n      <p class=\"banner__title\">\'.htmlspecialchars($lev2->getValue(\'name\')).\'</p></a>\r\n      \'.$banner[$md].\'\';\r\n      $md=$md+1;\r\n    }\r\n  endforeach;\r\n	$banner_main .= \'\';\r\n  echo \'\r\n    <div class=\"crt-item banner l-block\">\r\n      \'.$banner_main.\'\r\n      \'.rex::getProperty(\'lang_switch\').\'                        \r\n    </div>\';\r\n?> ','admin','admin','2018-07-24 21:34:53','2018-07-28 10:33:12','',0),
  (5,'Index-contenido','<div class=\"crt-item\">\r\n          <h2 class=\"main-main__title center\">Bienvenido a nuestro Sitio Web</h2>\r\n        </div>\r\n        <div class=\"crt-item\">\r\n          <p class=\"center\"><b>CROMOTÉCNICA BOLIVIANA</b>, es una de las empresas más reconocidas en el ramo de muebles metálicos cromados, electroplastificados y accesorios cromados para vehículos en la ciudad de Cochabamba. Contamos con años de experiencia en el mercado, con personal técnico altamente calificado, maquinarias y equipo modernos, dando la capacidad de interpretar las necesidades reales de la gente de la oficina, la casa, los comercios, los hospitales, escuelas, etc.</p>\r\n          <p class=\"center\">Superando las expectativas y obteniendo productos singularmente soñados. Estamos comprometidos en promover el desarrollo sustentable.</p>\r\n        </div>\r\n        <div class=\"crt-item\">\r\n          <h2 class=\"title-bar--metalic center\">PRODUCTOS NUEVOS</h2>\r\n        </div>\r\n        <div class=\"main-center\">\r\n          <div class=\"crt-item crt-container l-85\">\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://cloud10.todocoleccion.online/vintage-muebles/tc/2015/12/03/03/53111373.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Sillones para sala de espera</div>\r\n                  <p>Llegó la nueva silla sillon 4 sillones lote vintage despacho o sala de espera - oficina en acero cromado</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles hospitalarios</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img class=\"contain\" src=\"http://impulso.com.gt/wp-content/uploads/2014/07/29-SILLA-EJECUTIVA-ROMA-BASE-CROMADA.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Silla Ejecutiva Roma base cromada</div>\r\n                  <p>Llegó las nuevas sillas Ejecutivas Roma base cromada</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles de oficina</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://cloud10.todocoleccion.online/vintage-muebles/tc/2015/12/03/03/53111373.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Sillones para sala de espera</div>\r\n                  <p>Llegó la nueva silla sillon 4 sillones lote vintage despacho o sala de espera - oficina en acero cromado</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles de oficina</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://home.ripley.com.pe/Attachment/WOP/2064174522841/2064174522841_2.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Juego de comedor Dina Vidrio</div>\r\n                  <p>Llegó  el nuevo Juego de comedor Dina Vidrio 6 sillas</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles para el hogar</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>','<div class=\"crt-item\">\r\n          <h2 class=\"main-main__title center\">Bienvenido a nuestro Sitio Web</h2>\r\n        </div>\r\n        <div class=\"crt-item\">\r\n          <p class=\"center\"><b>CROMOTÉCNICA BOLIVIANA</b>, es una de las empresas más reconocidas en el ramo de muebles metálicos cromados, electroplastificados y accesorios cromados para vehículos en la ciudad de Cochabamba. Contamos con años de experiencia en el mercado, con personal técnico altamente calificado, maquinarias y equipo modernos, dando la capacidad de interpretar las necesidades reales de la gente de la oficina, la casa, los comercios, los hospitales, escuelas, etc.</p>\r\n          <p class=\"center\">Superando las expectativas y obteniendo productos singularmente soñados. Estamos comprometidos en promover el desarrollo sustentable.</p>\r\n        </div>\r\n        <div class=\"crt-item\">\r\n          <h2 class=\"title-bar--metalic center\">PRODUCTOS NUEVOS</h2>\r\n        </div>\r\n        <div class=\"main-center\">\r\n          <div class=\"crt-item crt-container l-85\">\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://cloud10.todocoleccion.online/vintage-muebles/tc/2015/12/03/03/53111373.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Sillones para sala de espera</div>\r\n                  <p>Llegó la nueva silla sillon 4 sillones lote vintage despacho o sala de espera - oficina en acero cromado</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles hospitalarios</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img class=\"contain\" src=\"http://impulso.com.gt/wp-content/uploads/2014/07/29-SILLA-EJECUTIVA-ROMA-BASE-CROMADA.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Silla Ejecutiva Roma base cromada</div>\r\n                  <p>Llegó las nuevas sillas Ejecutivas Roma base cromada</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles de oficina</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://cloud10.todocoleccion.online/vintage-muebles/tc/2015/12/03/03/53111373.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Sillones para sala de espera</div>\r\n                  <p>Llegó la nueva silla sillon 4 sillones lote vintage despacho o sala de espera - oficina en acero cromado</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles de oficina</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://home.ripley.com.pe/Attachment/WOP/2064174522841/2064174522841_2.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Juego de comedor Dina Vidrio</div>\r\n                  <p>Llegó  el nuevo Juego de comedor Dina Vidrio 6 sillas</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles para el hogar</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>','admin','admin','2018-07-27 21:48:10','2018-07-27 21:48:10','',0),
  (6,'Slide','<?php \r\n  $media = rex_media::get(\'REX_MEDIA[1]\');\r\n  /* Resize imagen */\r\n  $mediatype1420 = \'slide1420w\';\r\n  $mediatype960 = \'slide960w\';\r\n  $mediatype768 = \'slide768w\';\r\n  if (\"REX_MEDIA[1]\" != \'\'){\r\n    $slide960 = \'index.php?rex_media_type=\' . $mediatype1420 . \'&rex_media_file=REX_MEDIA[1]\';\r\n    $slide768 = \'index.php?rex_media_type=\' . $mediatype960 . \'&rex_media_file=REX_MEDIA[1]\';\r\n    $slide480 = \'index.php?rex_media_type=\' . $mediatype768 . \'&rex_media_file=REX_MEDIA[1]\';\r\n  }else{\r\n    $slide960 = \'\';\r\n    $slide768 = \'\';\r\n    $slide480 = \'\';\r\n  }\r\n?>\r\n<div class=\"main-banner\">\r\n  <div class=\"crt-container\">\r\n    <div class=\"crt-item l-75\">\r\n      <img class=\"main-banner__img\" src=\"<?= $slide960; ?>\"\r\n        srcset=\"<?= $slide960; ?> 480w,\r\n                <?= $slide768; ?> 768w,\r\n                <?= $slide480; ?> 960w\" \r\n        alt=\"<?php if ($media instanceof rex_media) { echo $media->getTitle();}?>\">\r\n      <div class=\"main-banner__data\">\r\n        <h1><?php if ($media instanceof rex_media) { echo $media->getTitle();}?></h1>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>','<fieldset>\r\n  <input type=\"hidden\" name=\"save\" value=\"1\">\r\n  <input type=\"hidden\" name=\"ctype\" value=\"1\">\r\n  <dl class=\"rex-form-group form-group\"><dt><label for=\"rex-id-meta-article-name\">Imagen para slider: </label></dt><dd>REX_MEDIA[id=1 widget=1]</dd></dl>\r\n</fieldset>','admin','','2018-07-26 11:01:35','0000-00-00 00:00:00','',0),
  (7,'Imagen de galeria','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\'); \r\n  /* Resize imagen */\r\n  $mediatype1 = \'thumb-210\';\r\n  $mediatype2 = \'thumb-850\';\r\n  if (\"REX_MEDIA[1]\" != \'\'){\r\n    $thumb = \'index.php?rex_media_type=\' . $mediatype1 . \'&rex_media_file=REX_MEDIA[1]\';\r\n  }else{\r\n    $thumb = \'https://vignette.wikia.nocookie.net/gangstar/images/8/84/Sin_imagen_disponible.jpg/revision/latest?cb=20131009183932&amp;path-prefix=es\';\r\n  }\r\n  if (\"REX_MEDIA[1]\" != \'\'){\r\n    $full = \'index.php?rex_media_type=\' . $mediatype2 . \'&rex_media_file=REX_MEDIA[1]\';\r\n  }else{\r\n    $full = \'https://vignette.wikia.nocookie.net/gangstar/images/8/84/Sin_imagen_disponible.jpg/revision/latest?cb=20131009183932&amp;path-prefix=es\';\r\n  }\r\n?>\r\n<div class=\"crt-item l-25 s-50 l-block\">           \r\n                <figure class=\"gallery__item\">                    \r\n                    <a href=\"<?= $full; ?>\" data-caption=\"<?php if ($media instanceof rex_media) { echo $media->getTitle();}?>\">\r\n                      <span class=\"gallery__item--description\">\r\n                        <i class=\"icon-zoom t1\"></i>\r\n                        <p><?php if ($media instanceof rex_media) { echo $media->getTitle();}?></p>\r\n                      </span>  \r\n                      <img src=\"<?= $thumb; ?>\" alt=\"<?php if ($media instanceof rex_media) { echo $media->getTitle();}?>\">                    \r\n                    </a>\r\n                 <!-- <figcaption>\r\n                    <p> </p>\r\n                  </figcaption> -->\r\n                </figure>\r\n              </div>','<?php\r\n  /*Obtencion de id para titulo de imagen*/\r\n  $media = rex_media::get(\'REX_MEDIA[1]\');\r\n?>\r\n<fieldset>\r\n  <input type=\"hidden\" name=\"save\" value=\"1\">\r\n  <input type=\"hidden\" name=\"ctype\" value=\"1\">  \r\n  <dl class=\"rex-form-group form-group\">\r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Titulo de producto:</label>\r\n    </dt>\r\n    <dd>\r\n      <p><?php if ($media instanceof rex_media) { echo $media->getTitle();} else { echo \'Agrege una imagen con titulo para visualizar\'; }?></p>\r\n    </dd>\r\n  </dl>\r\n  <dl class=\"rex-form-group form-group\">  \r\n    <dt>\r\n      <label for=\"rex-id-meta-article-name\">Subir Imagen: </label>\r\n    </dt>\r\n    <dd>REX_MEDIA[id=1 widget=1]</dd>    \r\n  </dl>\r\n  <!-- <div class=\"form-group\">\r\n    <label class=\"col-sm-2 control-label\">Descripcion: </label>\r\n    <div class=\"col-sm-10\">\r\n      <textarea class=\"redactorEditor2-basic\" name=\"REX_INPUT_VALUE[1]\">REX_VALUE[1]</textarea>\r\n    </div>\r\n  </div> -->\r\n</fieldset>','admin','admin','2018-07-28 11:02:31','2018-07-30 15:53:48','',0),
  (8,'Editor de texto','<div class=\"crt-item\">\r\n  REX_VALUE[id=\"1\" output=\"html\"]\r\n</div>','<textarea class=\"redactorEditor2-basic\" name=\"REX_INPUT_VALUE[1]\">REX_VALUE[1]</textarea>','admin','admin','2018-07-28 11:23:33','2018-08-07 18:03:37','',0);
/*!40000 ALTER TABLE `rex_module` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_module_action`;
CREATE TABLE `rex_module_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_redactor2_profiles`;
CREATE TABLE `rex_redactor2_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `urltype` varchar(50) NOT NULL,
  `minheight` smallint(5) unsigned NOT NULL,
  `maxheight` smallint(5) unsigned NOT NULL,
  `characterlimit` smallint(5) unsigned NOT NULL,
  `toolbarfixed` tinyint(1) unsigned NOT NULL,
  `shortcuts` tinyint(1) unsigned NOT NULL,
  `linkify` tinyint(1) unsigned NOT NULL,
  `redactor_plugins` text NOT NULL,
  `redactor_customplugins` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_redactor2_profiles` WRITE;
/*!40000 ALTER TABLE `rex_redactor2_profiles` DISABLE KEYS */;
INSERT INTO `rex_redactor2_profiles` VALUES 
  (1,'full','Standard Redactor-Konfiguration','relative',300,800,0,0,0,1,'anchorlink,alignment,blockquote,bold,cleaner,clips[Snippetname1=Snippettext1|Snippetname2=Snippettext2],deleted,emaillink,externallink,fontcolor[Weiss=#ffffff|Schwarz=#000000],fontfamily[Arial|Times],fontsize[12px|15pt|120%],format[Absatz Klein=p.small|Absatz Mittel=p.middle|Absatz Gross=p.big],fullscreen,groupheading[1|2|3|4|5|6],grouplink[email|external|internal|media|telephone],grouplist[unorderedlist|orderedlist|indent|outdent],heading1,heading2,heading3,heading4,heading5,heading6,horizontalrule,internallink,italic,media,medialink,orderedlist,paragraph,properties,redo,source,styles[code=Code|kbd=Shortcut|mark=Markiert|samp=Sample|var=Variable],sub,sup,table,telephonelink,textdirection,underline,undo,unorderedlist',''),
  (2,'basic','Editor de texto basico, esencial','relative',300,800,0,1,0,0,'alignment,bold,cleaner,deleted,fullscreen,grouplink[email|external],grouplist[unorderedlist|orderedlist|indent|outdent],horizontalrule,italic,paragraph,properties,redo,source,underline,undo','');
/*!40000 ALTER TABLE `rex_redactor2_profiles` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_template`;
CREATE TABLE `rex_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `active` tinyint(1) DEFAULT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `attributes` text,
  `revision` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_template` WRITE;
/*!40000 ALTER TABLE `rex_template` DISABLE KEYS */;
INSERT INTO `rex_template` VALUES 
  (1,'01.Inicio','REX_TEMPLATE[2]\r\n      <div class=\"crt-slider\">\r\n         REX_ARTICLE[4] <?php //SLIDER ?>              \r\n      </div>\r\n      <main class=\"crt-container main-main l-block\">\r\n        <div class=\"crt-item\">\r\n          <h2 class=\"main-main__title center\">Bienvenido a nuestro Sitio Web</h2>\r\n        </div>\r\n        <div class=\"crt-item\">\r\n          <p class=\"center\"><b>CROMOTÉCNICA BOLIVIANA</b>, es una de las empresas más reconocidas en el ramo de muebles metálicos cromados, electroplastificados y accesorios cromados para vehículos en la ciudad de Cochabamba. Contamos con años de experiencia en el mercado, con personal técnico altamente calificado, maquinarias y equipo modernos, dando la capacidad de interpretar las necesidades reales de la gente de la oficina, la casa, los comercios, los hospitales, escuelas, etc.</p>\r\n          <p class=\"center\">Superando las expectativas y obteniendo productos singularmente soñados. Estamos comprometidos en promover el desarrollo sustentable.</p>\r\n        </div>\r\n      </main>\r\n      <section class=\"section l-block\">\r\n        <div class=\"crt-item\">\r\n          <h2 class=\"title-bar--blue center\">PRODUCTOS NUEVOS</h2>\r\n        </div>\r\n        <div class=\"main-center\">\r\n          <div class=\"crt-item crt-container l-85\">\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://cloud10.todocoleccion.online/vintage-muebles/tc/2015/12/03/03/53111373.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Sillones para sala de espera</div>\r\n                  <p>Llegó la nueva silla sillon 4 sillones lote vintage despacho o sala de espera - oficina en acero cromado</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles hospitalarios</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img class=\"contain\" src=\"http://impulso.com.gt/wp-content/uploads/2014/07/29-SILLA-EJECUTIVA-ROMA-BASE-CROMADA.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Silla ejecutiva roma base cromada</div>\r\n                  <p>Llegó las nuevas sillas Ejecutivas Roma base cromada</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles de oficina</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://cloud10.todocoleccion.online/vintage-muebles/tc/2015/12/03/03/53111373.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Sillones para sala de espera</div>\r\n                  <p>Llegó la nueva silla sillon 4 sillones lote vintage despacho o sala de espera - oficina en acero cromado</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles de oficina</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-25\">\r\n              <div class=\"card\">\r\n                <div class=\"card__img\"><img src=\"https://home.ripley.com.pe/Attachment/WOP/2064174522841/2064174522841_2.jpg\"></div>\r\n                <div class=\"card__content\">\r\n                  <div class=\"t3 card__title center\">Juego de comedor Dina Vidrio</div>\r\n                  <p>Llegó  el nuevo Juego de comedor Dina Vidrio 6 sillas</p>\r\n                  <footer class=\"card__footer to-center\">\r\n                    <div class=\"card__button\"><span><b>Ver más: </b></span><a class=\"button--ghost--cta small\" href=\"#\">Muebles para el hogar</a></div>\r\n                  </footer>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </section>\r\n      <section class=\"crt-container main-main l-block\">\r\n        <div class=\"crt-container\">\r\n          <div class=\"crt-item\">\r\n            <h2 class=\"title-bar--blue center\">PROMOCIONES ESPECIALES</h2>\r\n          </div>\r\n          <div class=\"main-center crt-item crt-container\" id=\"gallery\">\r\n            <div class=\"crt-item l-50 l-block crt-container\">\r\n              <div class=\"crt-item m-60\">\r\n                <figure>\r\n                  <a href=\"https://ae01.alicdn.com/kf/HTB1qDBARpXXXXXdXpXXq6xXFXXXq/Ajuste-para-Ford-Edge-2012-2013-2014-parachoques-delantero-y-trasero-parabrisas-Parachoques-labio-protector-antideslizante.jpg_640x640.jpg\"><img src=\"https://ae01.alicdn.com/kf/HTB1qDBARpXXXXXdXpXXq6xXFXXXq/Ajuste-para-Ford-Edge-2012-2013-2014-parachoques-delantero-y-trasero-parabrisas-Parachoques-labio-protector-antideslizante.jpg_640x640.jpg\" alt=\"\"></a>\r\n                  <figcaption>\r\n                    <p> </p>\r\n                  </figcaption>\r\n                </figure>\r\n              </div>\r\n              <div class=\"crt-item m-40\">\r\n                <h2>Gran oferta de parachoques delateros y traseros.</h2>\r\n                <p>Gran oferta para Ford Edge 2012 2013 2014 parachoques delantero y trasero parabrisas Parachoques labio protector antideslizante ABS cromo acabado</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"crt-item l-50 l-block crt-container\">\r\n              <div class=\"crt-item m-60\">\r\n                <figure>\r\n                  <a href=\"https://industriascruzcentro.com/wp-content/uploads/2017/10/Pupitre-bipersonal.jpg\"><img src=\"https://industriascruzcentro.com/wp-content/uploads/2017/10/Pupitre-bipersonal.jpg\" alt=\"\"></a>\r\n                </figure>\r\n              </div>\r\n              <div class=\"crt-item m-40\">\r\n                <h2>En oferta pupitre bipersonal en madera</h2>\r\n                <p>Estructura en tubo redondo de 1 y ½ pulgada calibre 18.  Pintura en polvo electrostática. Tapa en triplex de 19 mm. enchapada en formica bocelada y perfil plástico color negro.</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </section>\r\n      <section class=\"section__first\">\r\n        <div class=\"crt-container\">\r\n          <div class=\"crt-item\">\r\n            <h2 class=\"title-bar--blue center\">NUESTROS SERVICIOS ESPECIALIZADOS</h2>\r\n          </div>\r\n          <div class=\"crt-item l-50 crt-container l-block\">\r\n            <div class=\"crt-item l-50\">\r\n              <h2>CROMADO DE MATERIALES</h2>\r\n              <p>Cromado de piezas: cromado de plastico, cromado de abs, cromado de polipropileno, cromado de polietileno, cromado de policarbonato, cromado de pvc, cromado de aluminio, cromado de hierro, cromado de latón, etc...</p>\r\n            </div>\r\n            <div class=\"crt-item l-50\">\r\n              <div class=\"card card--next-class\">\r\n                <div><a href=\"#\"><img src=\"https://cdn.ready-market.com/6/d65bba8a//Templates/pic/shiny_chrome.jpg?v=b92ea2be\"></a></div>\r\n                <!-- <div class=\"card__content\">\r\n                  <div class=\"card__title\"><a href=\"#\">SERVICIO DE CROMADO</a></div>\r\n                </div> -->\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"crt-item l-50 crt-container l-block\">\r\n            <div class=\"crt-item l-50\">\r\n              <h2>PINTURA ELECTROSTÁTICA</h2>\r\n              <p>Ofrecemos servicio de pintura electrostática para todo tipo de piezas metálicas y en aluminio con una amplia variedad de colores y texturas (lisas, mate o semi-mate), según el requerimiento del cliente. Nuestro moderno sistema logra recubrimientos más parejos penetrando incluso en zonas difíciles.</p>\r\n            </div>\r\n            <div class=\"crt-item l-50\">\r\n              <div class=\"card card--next-class\">\r\n                <div><a href=\"#\"><img src=\"https://images01.olx-st.com/ui/53/25/76/08/o_1495596096_934415382a7f6fd75f1516ea8925f5b6.jpg\"></a></div>\r\n                <!-- <div class=\"card__content\">\r\n                  <div class=\"card__title\"><a href=\"#\">SERVICIO DE PINTURA ELECTROSTÁTICA</a></div>\r\n                </div> -->\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </section>\r\n      <section class=\"section\">\r\n        <div class=\"crt-container\">\r\n          <div class=\"crt-item\">\r\n            <h2 class=\"title-bar--blue center\">ACCESORIOS PARA VEHICULOS</h2>\r\n          </div>\r\n          <div class=\"main-center crt-item crt-container\">\r\n            <div class=\"crt-item crt-container l-80\">\r\n              <div class=\"crt-item l-1-3 l-block\">\r\n                <div class=\"card\">\r\n                  <div class=\"card__img--circular\"><img src=\"http://www.lonascorcuera.cl/BarraAntivuelco/img/Barra-antivuelco-negra.jpg\"></div>\r\n                  <div class=\"card__content\">\r\n                    <div class=\"t3 card__title center\">BARRAS ANTI-VUELCOS</div>\r\n                    <p> </p>\r\n                    <footer class=\"card__footer to-center\">\r\n                      <div class=\"card__button\"> <a class=\"button--cta small\" href=\"#\">Ver más</a></div>\r\n                    </footer>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"crt-item l-1-3 l-block\">\r\n                <div class=\"card\">\r\n                  <div class=\"card__img--circular\"><img src=\"https://i.ebayimg.com/images/g/BiYAAMXQ74JTQ2~h/s-l300.jpg\"></div>\r\n                  <div class=\"card__content\">\r\n                    <div class=\"t3 card__title center\">BARRAS LATERALES</div>\r\n                    <p> </p>\r\n                    <footer class=\"card__footer to-center\">\r\n                      <div class=\"card__button\"> <a class=\"button--cta small\" href=\"#\">Ver más</a></div>\r\n                    </footer>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"crt-item l-1-3 l-block\">\r\n                <div class=\"card\">\r\n                  <div class=\"card__img--circular\"><img src=\"http://www.carryboy.cl/Content/products/30c0521d-fd6f-4c41-bf41-8da5259fa7d1.jpg\"></div>\r\n                  <div class=\"card__content\">\r\n                    <div class=\"t3 card__title center\">PISADERAS</div>\r\n                    <p> </p>\r\n                    <footer class=\"card__footer to-center\">\r\n                      <div class=\"card__button\"> <a class=\"button--cta small\" href=\"#\">Ver más									</a></div>\r\n                    </footer>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </section>\r\n\r\nREX_TEMPLATE[3]',1,'soporte-cromotecnica','soporte-cromotecnica','2018-08-21 11:51:39','2018-08-21 11:51:39','{\"ctype\":[],\"modules\":{\"1\":{\"0\":\"2\",\"1\":\"5\",\"2\":\"3\",\"3\":\"6\",\"all\":0}},\"categories\":{\"all\":\"1\"}}',0),
  (2,'02.Header','<?php\r\n$versionSite = \'1.6.1\';\r\n// error_reporting(0);\r\nerror_reporting(E_ALL);\r\n// Error report should only be active during development. Deavtivate (0) on a live website\r\n\r\n$page_is_visible = true;\r\n\r\n// Is current article offline?\r\nif (rex_article::getCurrent()->isOnline() == 0) {\r\n	$page_is_visible = false;\r\n}\r\n\r\n// is online_from_date newer than actual date?\r\nif (rex_article::getCurrent()->getValue(\'art_online_from\') != \'\' && rex_article::getCurrent()->getValue(\'art_online_from\') > time()) {\r\n	$page_is_visible = false;\r\n}\r\n\r\n// is online_from_date older than actual date\r\nif (rex_article::getCurrent()->getValue(\'art_online_to\') != \'\' && rex_article::getCurrent()->getValue(\'art_online_to\') < time()) {\r\n	$page_is_visible = false;\r\n}\r\n\r\n\r\n// Is User not logged in?\r\nif (!rex_backend_login::hasSession()) {\r\n	if ($page_is_visible == false) {\r\n		// redirect to 404 page\r\n		header (\'HTTP/1.1 301 Moved Permanently\');\r\n		header(\'Location: \'.rex_getUrl(rex_article::getNotFoundArticleId(), rex_clang::getCurrentId()));\r\n		exit();\r\n	}\r\n}\r\n\r\n// Necessary for input and output of module \"Tabs und Akkordions\"\r\nrex::setProperty(\'tabs\', new ArrayIterator());\r\n\r\n// set charset to utf8\r\nheader(\'Content-Type: text/html; charset=utf-8\');\r\n\r\n// setLocale is a language meta field, set your individual locale informations per language\r\nsetlocale (LC_ALL, rex_clang::getCurrent()->getValue(\'clang_setlocale\'));\r\n\r\n?>\r\n<!DOCTYPE html>\r\n<html lang=\"<?php echo rex_clang::getCurrent()->getCode(); ?>\" prefix=\"og: http://ogp.me/ns#\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"initial-scale=1.0, width=device-width\">\r\n    <meta name=\"designer\" content=\"Fernando Javier Averanga Aruquipa / nandes.ingsistemas@gmail.com\">   \r\n    <meta name=\"author\" content=\"\">\r\n<?php\r\n	// Use article title as title-Tag, unless a custom title-tag is set\r\n    if ($this->hasValue(\"art_title\") && $this->getValue(\"art_title\") != \"\") {\r\n		$title = htmlspecialchars($this->getValue(\'art_title\'));\r\n	} else {\r\n		$title = htmlspecialchars($this->getValue(\'name\'));\r\n	}\r\n\r\n	echo \'<title>\'.$title.\' | \' . rex::getServerName() . \' - Fábrica de muebles cromados y electro plastificados</title>\';\r\n\r\n        echo \'<meta property=\"og:title\" content=\"\'.$title.\' | \' . rex::getServerName() . \' - Fábrica de muebles cromados y electro plastificados\">\';\r\n\r\n	// Keywords and description\r\n	// If current article does not have keywords and description, take them from start article\r\n	$keywords = \"\";\r\n    if ($this->hasValue(\"art_keywords\") && $this->getValue(\"art_keywords\") != \"\") {\r\n        $keywords = $this->getValue(\"art_keywords\");\r\n    } else {\r\n        $home = new rex_article_content(rex_article::getSiteStartArticleId());\r\n        if ($home->hasValue(\"art_keywords\")) {\r\n            $keywords = $home->getValue(\'art_keywords\');\r\n        }\r\n    }\r\n\r\n    $description = \"MUEBLES PARA EL HOGAR,MUEBLES DE OFICINA, MUEBLES HOSPITALARIOS, MUEBLES EDUCATIVOS, ACCESORIOS CROMADOS DE VEHICULOS, PARACHOQUES Y PARRILLAS, SERVICIOS.\";\r\n    if ($this->hasValue(\"art_description\") && $this->getValue(\"art_description\") != \"\") {\r\n        $description = $this->getValue(\"art_description\");\r\n    } else {\r\n        $home = new rex_article_content(rex_article::getSiteStartArticleId());\r\n        if ($home->hasValue(\"art_description\")) {\r\n            $description = $home->getValue(\'art_description\');\r\n        }\r\n    }\r\n\r\n	echo \'\r\n	<meta name=\"keywords\" content=\"\'.htmlspecialchars($keywords).\'\">\';\r\n\r\n	echo \'\r\n	<meta name=\"description\" content=\"\'.htmlspecialchars($description).\'\">\';\r\n\r\n        echo \'\r\n	<meta property=\"og:description\" content=\"\'.htmlspecialchars($description).\'\">\';\r\n	?>\r\n    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Lato:400,400i,700|Yanone+Kaffeesatz:300,400,700\">\r\n    <link rel=\"stylesheet\" href=\"<?= rex_url::base(\'assets/css/styles.css?v=\' . $versionSite) ?>\">\r\n  </head>\r\n  <body class=\"Inicio\">\r\n    <div id=\"rex\">\r\n      <header class=\"main-header crt-container full\">\r\n        <div class=\"crt-item s-20 l-75 cross-end\">\r\n          <nav class=\"main-menu\" id=\"main-menu\">\r\n            <ul>\r\n              <li><a href=\"<?= rex_url::base(\'\') ?>\"><i class=\"icon-home\"></i>Inicio</a></li>\r\n              <li><a href=\"<?= rex_url::base(\'nosotros\') ?>\"><i class=\"icon-users\"></i>Nosotros</a></li>\r\n              <li><a href=\"<?= rex_url::base(\'contactos\') ?>\"><i class=\"icon-contact\"></i>Contactos</a></li>\r\n            </ul>\r\n            <?php\r\n						$path = explode(\"|\",$this->getValue(\"path\").$this->getValue(\"article_id\").\"|\");\r\n						$path1 = ((!empty($path[1])) ? $path[1] : \'\');\r\n						$path2 = ((!empty($path[2])) ? $path[2] : \'\');\r\n\r\n						$nav_main = \'\';\r\n\r\n						foreach (rex_category::getRootCategories() as $lev1) {\r\n\r\n							$hidden_ids = array(1,20);\r\n							// hide Home- and footer-article in navigation\r\n\r\n							$category_is_visible = true;\r\n							// only visible if online_from and online_to is ok\r\n							if ($lev1->getValue(\'art_online_from\') != \'\' && $lev1->getValue(\'art_online_from\') > time()) { $category_is_visible = false; }\r\n\r\n							if ($lev1->getValue(\'art_online_to\') != \'\' && $lev1->getValue(\'art_online_to\') < time()) { $category_is_visible = false; }\r\n\r\n\r\n							if ($lev1->isOnline(true) && (!in_array($lev1->getId(), $hidden_ids)) && $category_is_visible == true) {\r\n\r\n								if ($lev1->getId() == $path1) {\r\n									$nav_main .= \'\r\n									<li class=\"s-14 active\"><a class=\"button--metalic\" href=\"\'.$lev1->getUrl().\'\" >\'.htmlspecialchars($lev1->getValue(\'name\')).\'</a>\';\r\n								} else {\r\n										$nav_main .= \'\r\n									<li class=\"s-14\"><a class=\"button--metalic\" href=\"\'.$lev1->getUrl().\'\" >\'.htmlspecialchars($lev1->getValue(\'name\')).\'</a>\';\r\n								}\r\n\r\n								// 1st level start\r\n								$lev1Size = sizeof($lev1->getChildren());\r\n\r\n									if ($lev1Size != \"0\") {\r\n\r\n										$nav_main .= \'\r\n										<ul class=\"dropdown-menu\">\';\r\n										$nav_main .= \'\r\n										</ul>\';\r\n\r\n									}\r\n\r\n								$nav_main .= \'\r\n								</li>\';\r\n							}\r\n						}\r\n\r\n						echo \'						\r\n							<ul class=\"to-l\">\r\n								\'.$nav_main.\'\r\n								\'.rex::getProperty(\'lang_switch\').\'\r\n							</ul>\';\r\n					?>\r\n		  </nav>\r\n          <div class=\"main-menu-toggle to-l\" id=\"main-menu-toggle\"></div>\r\n		  <nav class=\"main-menu__social from-l\">\r\n			  <ul class=\"social\">\r\n				  <li><a class=\"icon-whatsapp\" href=\"https://api.whatsapp.com/send?phone=59170717027\" target=\"_blank\"></a></li>\r\n				  <li><a class=\"icon-facebook\" href=\"\" target=\"_blank\"></a></li>\r\n				  <li><a class=\"icon-youtube\" href=\"\" target=\"_blank\"></a></li>\r\n				</ul>\r\n		  </nav>          \r\n          <?php\r\n						$path = explode(\"|\",$this->getValue(\"path\").$this->getValue(\"article_id\").\"|\");\r\n						$path1 = ((!empty($path[1])) ? $path[1] : \'\');\r\n						$path2 = ((!empty($path[2])) ? $path[2] : \'\');\r\n\r\n						$nav_main = \'\';\r\n\r\n						foreach (rex_category::getRootCategories() as $lev1) {\r\n\r\n							$hidden_ids = array(1,20);\r\n							// hide Home- and footer-article in navigation\r\n\r\n							$category_is_visible = true;\r\n							// only visible if online_from and online_to is ok\r\n							if ($lev1->getValue(\'art_online_from\') != \'\' && $lev1->getValue(\'art_online_from\') > time()) { $category_is_visible = false; }\r\n\r\n							if ($lev1->getValue(\'art_online_to\') != \'\' && $lev1->getValue(\'art_online_to\') < time()) { $category_is_visible = false; }\r\n\r\n\r\n							if ($lev1->isOnline(true) && (!in_array($lev1->getId(), $hidden_ids)) && $category_is_visible == true) {\r\n\r\n								if ($lev1->getId() == $path1) {\r\n									$nav_main .= \'\r\n									<li class=\"s-14 active\"><a class=\"button--metalic\" href=\"\'.$lev1->getUrl().\'\" >\'.htmlspecialchars($lev1->getValue(\'name\')).\'</a>\';\r\n								} else {\r\n										$nav_main .= \'\r\n									<li class=\"s-14\"><a class=\"button--metalic\" href=\"\'.$lev1->getUrl().\'\" >\'.htmlspecialchars($lev1->getValue(\'name\')).\'</a>\';\r\n								}\r\n\r\n								// 1st level start\r\n								$lev1Size = sizeof($lev1->getChildren());\r\n\r\n									if ($lev1Size != \"0\") {\r\n\r\n										$nav_main .= \'\r\n										<ul class=\"dropdown-menu\">\';\r\n										$nav_main .= \'\r\n										</ul>\';\r\n\r\n									}\r\n\r\n								$nav_main .= \'\r\n								</li>\';\r\n							}\r\n						}\r\n\r\n						echo \'\r\n						<nav class=\"main-links from-l\">\r\n							<ul>\r\n								\'.$nav_main.\'\r\n								\'.rex::getProperty(\'lang_switch\').\'\r\n							</ul>\r\n						</nav>\';\r\n					?>\r\n        </div>\r\n		<div class=\"crt-item s-80 l-25 cross-center main-end\"><a class=\"main-logo\" href=\"<?= rex_url::base(\'\') ?>\"><img src=\"<?= rex_url::base(\'assets/images/cromotecnica-boliviana.png\') ?>\"/></a></div>\r\n      </header>',0,'soporte-cromotecnica','soporte-cromotecnica','2018-08-08 18:27:38','2018-08-08 18:27:38','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0),
  (3,'03.Footer','      <a class=\"button--goup button--metalic fade-out\" id=\"goup\">\r\n        <span class=\"t2\">IR ARRIBA</span>\r\n      </a>\r\n      <footer class=\"main-footer\">\r\n        <div class=\"l-block\"></div>\r\n        <div class=\"crt-container\">\r\n          <div class=\"crt-item l-30 m-100 s-100 main-footer__item l-block\">\r\n            <h2 class=\"t1 main-footer__title center\">PRODUCTOS Y SERVICIOS</h2>\r\n            <div class=\"main-footer__content main-center\">\r\n              <?php\r\n                $path = explode(\"|\",$this->getValue(\"path\").$this->getValue(\"article_id\").\"|\");\r\n                $path1 = ((!empty($path[1])) ? $path[1] : \'\');\r\n                $path2 = ((!empty($path[2])) ? $path[2] : \'\');\r\n\r\n                $nav_main = \'\';\r\n\r\n                foreach (rex_category::getRootCategories() as $lev1) {\r\n\r\n                  $hidden_ids = array(1,20);\r\n                  // hide Home- and footer-article in navigation\r\n\r\n                  $category_is_visible = true;\r\n                  // only visible if online_from and online_to is ok\r\n                  if ($lev1->getValue(\'art_online_from\') != \'\' && $lev1->getValue(\'art_online_from\') > time()) { $category_is_visible = false; }\r\n\r\n                  if ($lev1->getValue(\'art_online_to\') != \'\' && $lev1->getValue(\'art_online_to\') < time()) { $category_is_visible = false; }\r\n\r\n\r\n                  if ($lev1->isOnline(true) && (!in_array($lev1->getId(), $hidden_ids)) && $category_is_visible == true) {\r\n\r\n                    if ($lev1->getId() == $path1) {\r\n                      $nav_main .= \'\r\n                      <li class=\"active\"><a href=\"\'.$lev1->getUrl().\'\">\'.htmlspecialchars($lev1->getValue(\'name\')).\'</a>\';\r\n                    } else {\r\n                        $nav_main .= \'\r\n                      <li><a href=\"\'.$lev1->getUrl().\'\">\'.htmlspecialchars($lev1->getValue(\'name\')).\'</a>\';\r\n                    }                   \r\n\r\n                    $nav_main .= \'\r\n                    </li>\';\r\n                  }\r\n                }\r\n\r\n                echo \'\r\n                  <ul class=\"feature-list\">\r\n                    \'.$nav_main.\'\r\n                    \'.rex::getProperty(\'lang_switch\').\'\r\n                  </ul>\';\r\n              ?>\r\n            </div>\r\n          </div>\r\n          <div class=\"crt-item l-40 m-60 s-100 main-footer__item l-block\">\r\n            <h2 class=\"t1 main-footer__title center\">CROMOTÉCNICA BOLIVIANA</h2>\r\n            <div class=\"main-center\">\r\n              <div class=\"main-footer__content\">\r\n                <p><b class=\"text-cta\">FÁBRICA: </b>Colcapirhua Norte, Av. Blanco Galindo Km. 9,<br> c. La Paz # 872 (a tres cuadras de Cementerio Colcapirhua)<br><b>Tel.: </b>4379054 / <b>Cel:   </b><span class=\"t1\"><i class=\"icon-whatsapp\"> </i><span>70 71 70 27</span></span>     \r\n                  <span class=\"right l-block-top\">\r\n                    <a class=\"button--cta button--small\" href=\"<?= rex_url::base(\'\') ?>contactos\">\r\n                    Ver mapa<i class=\"icon-marker\"></i></a>\r\n                  </span>\r\n                </p>\r\n                <p><b class=\"text-cta\">VILLA BUSCH: </b>Av. Blanco Galindo # 854 Km. 4, acera Norte<br>Tel.: 4022081 / <b>Cel:   </b><span class=\"t1\"><i class=\"icon-whatsapp\"> </i>79 79 19 27 </span>\r\n                  <span class=\"right l-block-top\">\r\n                    <a class=\"button--cta button--small\" href=\"<?= rex_url::base(\'\') ?>contactos\">\r\n                    Ver mapa<i class=\"icon-marker\"></i></a>\r\n                  </span>\r\n                </p>\r\n                <p><b class=\"text-cta\">CENTRO: </b>c. 25 de Mayo # 539, entre Calama y Ladislao Cabrera <br><b>Tel.: </b>4504784 / <b>Cel:   </b><span class=\"t1\"><i class=\"icon-whatsapp\"> </i>72 22 88 88 </span>\r\n                    <span class=\"right l-block-top\">\r\n                      <a class=\"button--cta button--small\" href=\"<?= rex_url::base(\'\') ?>contactos\">\r\n                       Ver mapa<i class=\"icon-marker\"> </i></a>\r\n                    </span>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"crt-item l-30 m-40 s-100 main-footer__item l-block\">\r\n            <h2 class=\"t1 main-footer__title center\">Siguenos en:</h2>\r\n            <div class=\"main-footer__content main-center cross-center\">\r\n              <ul class=\"social\">\r\n                <li><a class=\"icon-whatsapp\" href=\"https://api.whatsapp.com/send?phone=59170717027\" target=\"_blank\"></a></li>\r\n                <li><a class=\"icon-facebook\" href=\"\" target=\"_blank\"></a></li>\r\n                <li><a class=\"icon-youtube\" href=\"\" target=\"_blank\"></a></li>\r\n              </ul>\r\n              <p class=\"center\">También puede contactarnos a:<br><a href=\"javascript:void(0)\" target=\"_blank\">vogp68@gmail.com</a></p>\r\n              <p class=\"center\">Todos los Derechos Reservados ®<br><b>CROMOTÉCNICA BOLIVIANA </b>© <span id=\"currentDate\"></span></p>\r\n              <p><a class=\"made-in\" href=\"\" target=\"_blank\">Diseño y Programación: AH! Publicidad</a></p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </footer>\r\n    </div>\r\n    <script src=\"<?= rex_url::base(\'assets/js/modernizr.js\') ?>\"></script>\r\n    <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCB69fT0i5alC-TQZ_7GrTxlm3k_hY8Yh8\"></script>\r\n    <script src=\"<?= rex_url::base(\'assets/js/script.js?v=\' . $versionSite) ?>\"></script>\r\n  </body>\r\n</html>',0,'admin','admin','2018-08-03 09:26:33','2018-08-03 09:26:33','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0),
  (4,'Plantilla estática ','REX_TEMPLATE[2]\r\n      <div class=\"crt-container main-section l-section\">\r\n        <main class=\"crt-item l-100 main-main\">\r\n            REX_ARTICLE[]\r\n        </main>\r\n      </div>\r\nREX_TEMPLATE[3]',1,'admin','admin','2018-07-25 10:36:03','2018-07-25 10:36:03','{\"ctype\":[],\"modules\":{\"1\":{\"0\":\"2\",\"1\":\"3\",\"2\":\"1\",\"all\":0}},\"categories\":{\"all\":\"1\"}}',0),
  (5,'Complementos','REX_TEMPLATE[2]\r\n      <div class=\"crt-slider\">\r\n         REX_ARTICLE[4] <?php //SLIDER ?>              \r\n      </div>\r\n	  <main class=\"crt-container main-main l-block\">\r\n         REX_ARTICLE[1]\r\n      </main>\r\nREX_TEMPLATE[3]',1,'admin','admin','2018-07-26 17:59:30','2018-07-26 17:59:30','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0),
  (6,'Plantilla dinámica ','REX_TEMPLATE[2]\r\n      <div class=\"crt-container main-section l-section\">\r\n        <main class=\"crt-item l-100 main-main crt-container\">\r\n            <div class=\"crt-item\">\r\n              <h2 class=\"main-main__title center\"><?php echo rex_category::getCurrent()->getname() ?><h2>\r\n            </div>     \r\n            REX_ARTICLE[]           \r\n            <?php       \r\n              $path = explode(\"|\",$this->getValue(\"path\").$this->getValue(\"article_id\").\"|\");\r\n              $path1 = ((!empty($path[1])) ? $path[1] : \'\');\r\n              $path2 = ((!empty($path[2])) ? $path[2] : \'\');\r\n              $section_main = \'\';\r\n                    // START 2nd level categories\r\n                    foreach (rex_category::getCurrent()->getChildren() as $lev2):\r\n\r\n                      $category_is_visible = true;\r\n                      // only visible if online_from and online_to is ok\r\n                      if ($lev2->getValue(\'art_online_from\') != \'\' && $lev2->getValue(\'art_online_from\') > time()) { $category_is_visible = false; }\r\n\r\n                      if ($lev2->getValue(\'art_online_to\') != \'\' && $lev2->getValue(\'art_online_to\') < time()) { $category_is_visible = false; }\r\n\r\n                      if ($lev2->isOnline(true) && $category_is_visible == true) {\r\n                        $section_main .= \'\r\n                        <h3 id=\"\'.$lev2->getId().\'\" class=\"title-bar--blue center\">\'.htmlspecialchars($lev2->getValue(\'name\')).\'</h3>\r\n                        <div class=\"crt-container\" id=\"gallery\">\r\n                          \'.rex_var_article::getArticle($lev2->getId(), -1, null). \r\n                        \'</div>\';\r\n                      }\r\n                    endforeach;\r\n										$section_main .= \'\';\r\n                    echo \'\r\n                      <div class=\"crt-item\">\r\n                        \'.$section_main.\'\r\n                        \'.rex::getProperty(\'lang_switch\').\'                        \r\n                      </div>\';\r\n             ?>\r\n        </main>\r\n      </div>      \r\nREX_TEMPLATE[3]',1,'admin','admin','2018-08-08 10:12:56','2018-08-08 10:12:56','{\"ctype\":[],\"modules\":{\"1\":{\"all\":\"1\"}},\"categories\":{\"all\":\"1\"}}',0);
/*!40000 ALTER TABLE `rex_template` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_user_role`;
CREATE TABLE `rex_user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `perms` text NOT NULL,
  `createuser` varchar(255) NOT NULL,
  `updateuser` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL,
  `updatedate` datetime NOT NULL,
  `revision` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `rex_user_role` WRITE;
/*!40000 ALTER TABLE `rex_user_role` DISABLE KEYS */;
INSERT INTO `rex_user_role` VALUES 
  (1,'Cromotécnica Adminstrador','','{\"general\":\"|quick_navigation[history]|quick_navigation[]|media_manager[]|quick_navigation[all_changes]|\",\"options\":\"|moveSlice[]|publishCategory[]|copyContent[]|article2startarticle[]|article2category[]|copyArticle[]|moveArticle[]|moveCategory[]|publishArticle[]|\",\"extras\":null,\"clang\":\"|1|\",\"media\":\"all\",\"structure\":\"all\",\"modules\":\"all\",\"yform_manager_table\":\"all\"}','admin','admin','2018-08-07 11:47:37','2018-08-07 11:47:37',0);
/*!40000 ALTER TABLE `rex_user_role` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `rex_yform_email_template`;
CREATE TABLE `rex_yform_email_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `mail_from` varchar(255) NOT NULL,
  `mail_from_name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `body_html` text NOT NULL,
  `attachments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yform_field`;
CREATE TABLE `rex_yform_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL,
  `prio` int(11) NOT NULL,
  `type_id` varchar(100) NOT NULL,
  `type_name` varchar(100) NOT NULL,
  `list_hidden` tinyint(1) NOT NULL,
  `search` tinyint(1) NOT NULL,
  `name` text NOT NULL,
  `label` text NOT NULL,
  `not_required` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yform_history`;
CREATE TABLE `rex_yform_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL,
  `dataset_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dataset` (`table_name`,`dataset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yform_history_field`;
CREATE TABLE `rex_yform_history_field` (
  `history_id` int(11) NOT NULL,
  `field` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`history_id`,`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yform_table`;
CREATE TABLE `rex_yform_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `list_amount` tinyint(3) unsigned NOT NULL DEFAULT '50',
  `list_sortfield` varchar(255) NOT NULL DEFAULT 'id',
  `list_sortorder` enum('ASC','DESC') NOT NULL DEFAULT 'ASC',
  `prio` int(11) NOT NULL,
  `search` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `export` tinyint(1) NOT NULL,
  `import` tinyint(1) NOT NULL,
  `mass_deletion` tinyint(1) NOT NULL,
  `mass_edit` tinyint(1) NOT NULL,
  `history` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_name` (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yrewrite_alias`;
CREATE TABLE `rex_yrewrite_alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias_domain` varchar(255) NOT NULL,
  `domain_id` int(11) NOT NULL,
  `clang_start` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yrewrite_domain`;
CREATE TABLE `rex_yrewrite_domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  `mount_id` int(11) NOT NULL,
  `start_id` int(11) NOT NULL,
  `notfound_id` int(11) NOT NULL,
  `clangs` varchar(255) NOT NULL,
  `clang_start` int(11) NOT NULL,
  `clang_start_hidden` tinyint(1) NOT NULL,
  `robots` text NOT NULL,
  `title_scheme` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rex_yrewrite_forward`;
CREATE TABLE `rex_yrewrite_forward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `article_id` int(11) NOT NULL,
  `clang` int(11) NOT NULL,
  `extern` varchar(255) NOT NULL,
  `media` varchar(255) NOT NULL,
  `movetype` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS = 1;