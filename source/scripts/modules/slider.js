export const options_slider = {
  container: '.crt-slider',
  mode: 'gallery',
  controlsText: [
    '<i class="icon-left"></i>',
    '<i class="icon-right"></i>'
  ],
  autoplayText: [
    '<i class="icon-play"></i>',
    '<i class="icon-pause"></i>'
  ],
  autoplay: true,
  speed: 2000,
  lazyload: true,
  animateIn: 'fadeIn',
  animateOut: 'flipOutX'
};