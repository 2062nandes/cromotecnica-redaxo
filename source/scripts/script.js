import { menuToggle } from "./modules/menu"
import { getdate } from "./modules/date"
import { edTabs } from "./modules/tabs"
import { goups } from "./modules/goup"
// import { videoSize } from "./modules/video"

// Slider VanillaJS (https://github.com/ganlanyuan/tiny-slider)
import { tns } from "./libraries/tinyslider"
import { galleryBox } from "./libraries/baguettebox"
import { options_slider } from "./modules/slider"

// import Data Vue
import { menuinicio, mainmenu} from './data/menus'
import { contactform } from "./data/contact-form"

import Vue from 'vue/dist/vue.min'
// import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'
Vue.use(VueResource);

// Vue Components
import { googleMap } from './components/googlemaps'
Vue.component('google-map', googleMap)

const rex = new Vue({
  el: '#rex',
  data: {
    path_media: '/assets/',
    path_page: '/',
    menuinicio,
    mainmenu,
    formSubmitted: false,
    vue: contactform,
  },
  mounted: function () {
    window.edTabs = edTabs;
    const slider = tns(options_slider);
    /* Images Desplegables  */
    galleryBox()
    // smothscroll
    goups()
    menuToggle()
    getdate()
  }
})