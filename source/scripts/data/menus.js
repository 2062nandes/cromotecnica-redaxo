export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: 'nosotros',
    icon: 'icon-us'
  },
  {
    title: 'Contactos',
    href: 'contactos',
    icon: 'icon-contact'
  }
]
export const mainmenu = [
  {
    title: 'Muebles para el hogar',
    href: '',
    submenu: [
      {
        title: 'Juegos de comedor',
        id: ''
      },
      {
        title: 'Juegos de living',
        id: ''
      },
      {
        title: 'Mesas centrales de living',
        id: ''
      },
      {
        title: 'Mesas esquineras de living',
        id: ''
      },
      {
        title: 'Sillas',
        // (HAY VARIOS MODELOS, TIFANI ES SÓLO UNO)
        id: ''
      },
      {
        title: 'Taburetes',
        id: ''
      },
    ]
  },
  {
    title: 'Muebles de oficina',
    href: '',
    submenu: [
      {
        title: 'Escritorios',
        id: ''
      },
      {
        title: 'Mesas de reuniones',
        id: ''
      },
      {
        title: 'Sillas ejecutivas',
        id: ''
      },
      {
        title: 'Sillones',
        id: ''
      },
    ]
  },
  {
    title: 'Muebles hospitalarios',
    href: '',
    submenu: [
      {
        title: 'Sillas de espera',
        id: ''
      },
      {
        title: 'Butacas(Tandems)',
        id: ''
      },
      {
        title: 'Camillas',
        id: ''
      },
      {
        title: 'Camas',
        id: ''
      },
      {
        title: 'Biombos(Separadores)',
        id: ''
      },
    ]
  },
  {
    title: 'Muebles educativos',
    href: '',
    submenu: [
      {
        title: 'Pupitres',
        id: ''
      },
      {
        title: 'Mesas de estudio',
        id: ''
      },
      {
        title: 'Sillas escolares',
        id: ''
      },
      {
        title: 'Mesas de trabajo',
        id: ''
      },
    ]
  },
  {
    title: 'Accesorios cromados de vehiculos',
    href: '',
    submenu: [
      {
        title: 'Barras antivuelcos',
        id: ''
      },
      {
        title: 'Barras laterales',
        id: ''
      },
      {
        title: 'Parachoques delanteros',
        id: ''
      },
      {
        title: 'Parachoques traseros',
        id: ''
      },
      {
        title: 'Pisaderas',
        id: ''
      },
      {
        title: 'Parrillas',
        id: ''
      },
    ]
  },
  {
    title: 'Parachoques y Parrillas',
    href: '',
    submenu: [
      {
        title: 'Parachoques',
        id: ''
      },
      {
        title: 'Parrillas',
        id: ''
      },
    ]
  },
  {
    title: 'Servicios',
    href: '',
    submenu: [
      {
        title: 'Cromados',
        id: ''
      },
      {
        title: 'Pintura electrostática',
        id: ''
      },
    ]
  },
]